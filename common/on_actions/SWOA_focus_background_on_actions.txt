on_actions = {
	on_startup = {
		effect = {
			ENG = {
				set_variable = { show_background = 1 }
			}
			FRA = {
				set_variable = { show_background = 2 }
			}
			GER = {
				set_variable = { show_background = 3 }
			}
			ITA = {
				set_variable = { show_background = 4 }
			}
			JAP = {
				set_variable = { show_background = 5 }
			}
			SOV = {
				set_variable = { show_background = 6 }
			}
			USA = {
				set_variable = { show_background = 7 }
			}
			CAN = {
				set_variable = { show_background = 12 }
			}

			CHI = {
				set_variable = { show_background = 12 }
			}
			RAJ = {
				set_variable = { show_background = 12 }
			}
			AST = {
				set_variable = { show_background = 12 }
			}
			YUG = {
				set_variable = { show_background = 12 }
			}
			GRE = {
				set_variable = { show_background = 12 }
			}
			
			
			HUN = {
				set_variable = { show_background = 11 }
			}
			ROM = {
				set_variable = { show_background = 11 }
			}
			BUL = {
				set_variable = { show_background = 11 }
			}
			SPR = {
				set_variable = { show_background = 11 }
			}
			FIN = {
				set_variable = { show_background = 11 }
			}
		}
	}
}

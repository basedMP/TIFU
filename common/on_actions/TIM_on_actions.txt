on_actions = {
    on_startup = {
        effect = {
            BKB = { #remove clutter
                #country_event = { id = tim.1937     days = 365 }
                country_event = { id = tim.19375    days = 515 }
                country_event = { id = tim.1938     days = 730 }
                #country_event = { id = tim.1939     days = 1095 }
                #country_event = { id = tim.1940     days = 1460 }
                HMM_apply_intel = yes
            }

            every_country = {
                if = {
                    limit = {
                        is_neutral_nation = yes
                        is_allies_member_ai = yes
                    }
                    country_lock_all_division_template = yes
                }
                add_ideas = no_tradeback
                set_rule = {
                    can_access_market = no
                }
                if = {
                    limit = { #Playable
                        OR = {
                            is_allied = yes
                            is_axis = yes
                            original_tag = SOV
                            original_tag = JAP
                        }
                    }
                    if = {
                        limit = { is_major = yes}
                        add_ideas = TIFU_major
                    }
                    else = {
                        add_ideas = TIFU_minor
                    }
                    add_ideas = { peacetime_training }
                    set_technology = {
                        tech_mountaineers = 1
                        marines = 1
                        paratroopers = 0
                        popup = no
                    }
                    remove_ideas = MTG_naval_treaty_adherent
                    remove_ideas = MTG_naval_treaty_cheating
                    remove_ideas = MTG_naval_treaty_cheating_reduced
                    remove_ideas = MTG_naval_treaty_adherent_reduced
                    #country_event = { id = tim.1 } #dont trigger cause ape
                    country_lock_all_division_template = no
                }

                # ALLIED TRADE
                if = { 
                    limit = {
                        OR = {
                            is_allied = yes
                            original_tag = SOV
                        }
                    }
                    add_relation_modifier = {
                        target = USA
                        modifier = team_trading
                    }
                    add_relation_modifier = {
                        target = ENG
                        modifier = team_trading
                    }
                    add_relation_modifier = {
                        target = FRA
                        modifier = team_trading
                    }
                    add_relation_modifier = {
                        target = RAJ
                        modifier = team_trading
                    }
                    add_relation_modifier = {
                        target = AST
                        modifier = team_trading
                    }

                    add_relation_modifier = {
                        target = SOV
                        modifier = team_trading
                    }
                }

                # AXIS TRADE
                if = { 
                    limit = {
                        OR = {
                            is_axis = yes
                            original_tag = JAP
                        }
                    }
                    add_relation_modifier = {
                        target = GER
                        modifier = team_trading
                    }
                    add_relation_modifier = {
                        target = ITA
                        modifier = team_trading
                    }
                    add_relation_modifier = {
                        target = HUN
                        modifier = team_trading
                    }
                    add_relation_modifier = {
                        target = ROM
                        modifier = team_trading
                    }
                    add_relation_modifier = {
                        target = BUL
                        modifier = team_trading
                    }
                    add_relation_modifier = {
                        target = FIN
                        modifier = team_trading
                    }
                    add_relation_modifier = {
                        target = SPR
                        modifier = team_trading
                    }

                    add_relation_modifier = {
                        target = JAP
                        modifier = team_trading
                    }
                }
            }

            JAP = { 537 = { add_resource = { type = oil amount = 70 } } }
            SOV = { add_war_support = 0.05 } #turkish event
            FRA = { country_event = { id = election.3 days = 155 } } #mob up
            if = {
                limit = { has_game_rule = { rule = TIFU_balanced option = yes }  }
                ITA = {
                    annex_country = {
                        target = AFA
                        transfer_troops = no
                    }
                    annex_country = {
                        target = ETH
                        transfer_troops = no
                    }
                    clr_global_flag = second_italo_ethiopian_war_flag
                }
            }
            #    ██████╗██╗██╗   ██╗██████╗  ██████╗  ██████╗ ███████╗████████╗
            #   ██╔════╝██║██║   ██║██╔══██╗██╔═══██╗██╔═══██╗██╔════╝╚══██╔══╝
            #   ██║     ██║██║   ██║██████╔╝██║   ██║██║   ██║███████╗   ██║   
            #   ██║     ██║╚██╗ ██╔╝██╔══██╗██║   ██║██║   ██║╚════██║   ██║   
            #   ╚██████╗██║ ╚████╔╝ ██████╔╝╚██████╔╝╚██████╔╝███████║   ██║   
            #    ╚═════╝╚═╝  ╚═══╝  ╚═════╝  ╚═════╝  ╚═════╝ ╚══════╝   ╚═╝   

            USA = {
                capital_scope = {
                    add_resource = {
                        type = steel
                        amount = 200
                    }
                    add_resource = {
                        type = chromium
                        amount = 50
                    }
                }
            }
            JAP = {
                capital_scope = {
                    add_extra_state_shared_building_slots = 15
                    add_building_construction = {
                        type = industrial_complex
                        level = 15
                        instant_build = yes
                    }
                    add_resource = {
                        type = steel
                        amount = 200
                    }
                    add_resource = {
                        type = chromium
                        amount = 50
                    }
                }
            }

            ENG = {
                capital_scope = {
                    add_extra_state_shared_building_slots = 5
                    add_building_construction = {
                        type = industrial_complex
                        level = 5
                        instant_build = yes
                    }
                    add_resource = {
                        type = steel
                        amount = 100
                    }
                    add_resource = {
                        type = chromium
                        amount = 50
                    }
                }
            }
            ITA = {
                capital_scope = {
                    add_resource = {
                        type = steel
                        amount = 100
                    }
                    add_resource = {
                        type = chromium
                        amount = 50
                    }
                }
            }

            #   ██████╗ ██████╗ ███████╗ ██████╗ ██████╗ ██╗███╗   ██╗██████╗ 
            #   ██╔══██╗██╔══██╗██╔════╝██╔════╝ ██╔══██╗██║████╗  ██║██╔══██╗
            #   ██████╔╝██████╔╝█████╗  ██║  ███╗██████╔╝██║██╔██╗ ██║██║  ██║
            #   ██╔═══╝ ██╔══██╗██╔══╝  ██║   ██║██╔══██╗██║██║╚██╗██║██║  ██║
            #   ██║     ██║  ██║███████╗╚██████╔╝██║  ██║██║██║ ╚████║██████╔╝
            #   ╚═╝     ╚═╝  ╚═╝╚══════╝ ╚═════╝ ╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝╚═════╝ 

            FRA = { FRA_charles_de_gaulle = { add_unit_leader_trait = panzer_leader } } 

            if = {
                limit = { has_game_rule = { rule = TIFU_balanced option = yes } }

                ############
                ## ALLIES ##
                ############

                FRA = {
                    FRA_charles_de_gaulle = {
                        add_skill_level = 2
                        add_attack = 3 
                        add_planning = -2 
                        add_defense = -1
                        add_unit_leader_trait = organizer
                        add_unit_leader_trait = trait_engineer
                        add_unit_leader_trait = trickster
                        add_unit_leader_trait = trait_mountaineer
                    }
                } 
                
                ##########
                ## AXIS ##
                ##########

                GER = {
                    GER_sepp_dietrich = {
                        add_skill_level = 2
                        add_attack = 3 
                        add_planning = -2 
                        add_defense = -1
                        add_unit_leader_trait = panzer_leader
                        add_unit_leader_trait = cavalry_leader
                        add_unit_leader_trait = organizer
                        add_unit_leader_trait = trait_engineer
                        add_unit_leader_trait = trickster
                        add_unit_leader_trait = ranger
                    }
                    GER_hasso_von_manteuffel = {
                        add_skill_level = 2
                        add_attack = 2
                        add_logistics = 3
                        add_unit_leader_trait = panzer_leader
                        add_unit_leader_trait = cavalry_leader
                        add_unit_leader_trait = organizer
                        add_unit_leader_trait = trait_engineer
                        add_unit_leader_trait = trickster
                        add_unit_leader_trait = ranger
                        add_unit_leader_trait = commando
                    }
                    GER_erwin_rommel = {
                        add_logistics = 3
                        add_unit_leader_trait = organizer
                        add_unit_leader_trait = infantry_leader
                    }
                }
                

                ROM = {
                    ROM_gheorghe_avramescu = {
                        add_unit_leader_trait = infantry_leader
                        add_unit_leader_trait = naval_invader
                    }
                }

                HUN = {
                    HUN_ivan_hindy = {
                        add_unit_leader_trait = panzer_leader
                        add_unit_leader_trait = ranger
                        add_attack = 1
                    }
                }

                SPR = {
                    SPR_mohamed_meziane = {
                        add_skill_level = 2
                        add_attack = 1
                        add_planning = 2
                        add_defense = 1
                        add_unit_leader_trait = panzer_leader
                        add_unit_leader_trait = cavalry_leader
                        add_unit_leader_trait = organizer
                        add_unit_leader_trait = trickster
                        add_unit_leader_trait = ranger
                    }
                    SPA_francisco_franco = {
                        add_skill_level = 3
                        add_attack = 3
                        add_defense = 3
                        add_planning = 2
                        add_logistics = 3
                        add_unit_leader_trait = panzer_leader
                        add_unit_leader_trait = cavalry_leader
                        add_unit_leader_trait = organizer
                        add_unit_leader_trait = hill_fighter
                    }
                }

                ITA = {
                    ITA_giovanni_messe = {
                        add_skill_level = 2
                        add_planning = 2
                        add_logistics = 1
                        add_attack = 1
                        add_defense = 1
                        add_unit_leader_trait = organizer
                        add_unit_leader_trait = infantry_leader
                        add_unit_leader_trait = trickster
                        add_unit_leader_trait = cavalry_leader
                        add_unit_leader_trait = panzer_leader
                        add_unit_leader_trait = trait_engineer
                        add_unit_leader_trait = trait_mountaineer
                        add_unit_leader_trait = desert_fox
                    } 
                    ITA_sebastiano_visconti_prasca = {
                        add_skill_level = 5
                        add_attack = 2
                        add_unit_leader_trait = organizer
                        add_unit_leader_trait = infantry_leader
                        add_unit_leader_trait = panzer_leader
                        add_unit_leader_trait = cavalry_leader
                        add_unit_leader_trait = trait_mountaineer
                    }
                    ITA_angelo_iachino = {
                        add_skill_level = 2
                        add_unit_leader_trait = ironside
                        add_unit_leader_trait = fleet_protector
                    }
                }

                #########
                # JAPAN #
                #########

                JAP = { #cause jap still gets china grind
                    JAP_tomoyuki_yamashita = {
                        add_skill_level = 1
                        add_planning = 2
                        add_attack = 1
                        add_unit_leader_trait = organizer
                        add_unit_leader_trait = infantry_leader
                        add_unit_leader_trait = trickster
                        add_unit_leader_trait = cavalry_leader
                        add_unit_leader_trait = trait_engineer
                        add_unit_leader_trait = trait_mountaineer
                        add_unit_leader_trait = ranger
                    }

                    JAP_shizuichi_tanaka = {
                        add_skill_level = 2
                        add_planning = 2
                        add_attack = 2
                        add_unit_leader_trait = organizer
                        add_unit_leader_trait = infantry_leader
                        add_unit_leader_trait = trait_engineer
                        add_unit_leader_trait = trait_mountaineer
                        add_unit_leader_trait = ranger
                        add_unit_leader_trait = commando
                    }
                }

                ##########
                # SOVIET #
                ##########

                SOV = {
                    SOV_konstantin_rokossovsky = {
                        add_skill_level = 2
                        add_attack = 1
                        add_defense = 1
                        add_logistics = 1
                        add_unit_leader_trait = panzer_leader
                        add_unit_leader_trait = organizer
                        add_unit_leader_trait = trickster
                        add_unit_leader_trait = ranger
                        add_unit_leader_trait = trait_engineer
                    }
                    SOV_georgy_zhukov = {
                        add_skill_level = 1
                        add_attack = 2
                        add_defense = 1
                        add_logistics = 1
                        add_unit_leader_trait = panzer_leader
                        add_unit_leader_trait = cavalry_leader
                        add_unit_leader_trait = trait_engineer
                        add_unit_leader_trait = trickster
                        add_unit_leader_trait = ranger
                    }
                    SOV_aleksandr_vasilevsky = {
                        add_logistics = 4
                        add_planning = -1
                        add_unit_leader_trait = organizer
                        add_unit_leader_trait = infantry_leader
                    }
	            }
            }
            #   ██╗   ██╗ ██████╗ ██╗     ███╗   ██╗███████╗
            #   ██║   ██║██╔═══██╗██║     ████╗  ██║██╔════╝
            #   ██║   ██║██║   ██║██║     ██╔██╗ ██║███████╗
            #   ╚██╗ ██╔╝██║   ██║██║     ██║╚██╗██║╚════██║
            #    ╚████╔╝ ╚██████╔╝███████╗██║ ╚████║███████║
            #     ╚═══╝   ╚═════╝ ╚══════╝╚═╝  ╚═══╝╚══════╝
            
            else = {
                
                GER = { add_ideas = HMM_vol_major } # 5 + 3
                ITA = { add_ideas = HMM_vol_major } # 5 + 3
                JAP = { add_ideas = HMM_vol_major } # 5
                HUN = { add_ideas = HMM_vol_minor } # 2
                ROM = { add_ideas = HMM_vol_minor } # 2 
                BUL = { add_ideas = HMM_vol_minor } # 2
                
                #SPR 187

                SOV = { add_ideas = HMM_vol_major } # 5 + 3
                FRA = { add_ideas = HMM_vol_major } # 5 + 3
            }
            FIN = { add_ideas = HMM_vol_vol } # 187

            #   ███████╗████████╗ ██████╗  ██████╗██╗  ██╗██████╗ ██╗██╗     ███████╗
            #   ██╔════╝╚══██╔══╝██╔═══██╗██╔════╝██║ ██╔╝██╔══██╗██║██║     ██╔════╝
            #   ███████╗   ██║   ██║   ██║██║     █████╔╝ ██████╔╝██║██║     █████╗  
            #   ╚════██║   ██║   ██║   ██║██║     ██╔═██╗ ██╔═══╝ ██║██║     ██╔══╝  
            #   ███████║   ██║   ╚██████╔╝╚██████╗██║  ██╗██║     ██║███████╗███████╗
            #   ╚══════╝   ╚═╝    ╚═════╝  ╚═════╝╚═╝  ╚═╝╚═╝     ╚═╝╚══════╝╚══════╝
			LIT = {
				add_equipment_to_stockpile = { type = infantry_equipment_1 amount = 25000 }
				add_equipment_to_stockpile = { type = support_equipment_1 amount = 300 }
				add_equipment_to_stockpile = {
					type = train_equipment_1
					amount = 30
				}
			}
            EST = {
				add_equipment_to_stockpile = { type = infantry_equipment_1 amount = 25000 }
				add_equipment_to_stockpile = { type = support_equipment_1 amount = 300 }
				add_equipment_to_stockpile = {
					type = train_equipment_1
					amount = 30
				}
			}
            LAT = {
				add_equipment_to_stockpile = { type = infantry_equipment_1 amount = 25000 }
				add_equipment_to_stockpile = { type = support_equipment_1 amount = 300 }
				add_equipment_to_stockpile = {
					type = train_equipment_1
					amount = 30
				}
			}
            POL = {
				add_equipment_to_stockpile = { type = infantry_equipment_1 amount = 50000 }
				add_equipment_to_stockpile = { type = support_equipment_1 amount = 800 }
				add_equipment_to_stockpile = { type = artillery_equipment_1 amount = 200 }
				add_equipment_to_stockpile = {
					type = train_equipment_1
					amount = 20
				}
			}
			HOL = {
				add_equipment_to_stockpile = { type = infantry_equipment_1 amount = 30000 }
				add_equipment_to_stockpile = { type = artillery_equipment_1 amount = 420 }
				add_equipment_to_stockpile = {
					type = train_equipment_1
					amount = 37
				}
				add_war_support = 0.05
			}
			DEN = {
				add_equipment_to_stockpile = { type = infantry_equipment_1 amount = 12000 }
				add_equipment_to_stockpile = { type = support_equipment_1 amount = 1800 }
				add_war_support = 0.05
			}
			NOR = {
				add_equipment_to_stockpile = { type = infantry_equipment_1 amount = 18000 }
				add_equipment_to_stockpile = { type = support_equipment_1 amount = 1500 }
				add_equipment_to_stockpile = { type = artillery_equipment_1 amount = 400 }
				add_equipment_to_stockpile = {
					type = train_equipment_1
					amount = 40
				}
				add_war_support = 0.05
			}
			BEL = {
				add_equipment_to_stockpile = { type = infantry_equipment_1 amount = 25000 }
				add_equipment_to_stockpile = { type = infantry_equipment_0 amount = 1000 }
				add_equipment_to_stockpile = { type = support_equipment_1 amount = 2000 }
				add_equipment_to_stockpile = { type = artillery_equipment_1 amount = 100 }
				add_equipment_to_stockpile = { type = motorized_equipment_1 amount = 2200 }
				add_equipment_to_stockpile = {
					type = train_equipment_1
					amount = 50
				}
			}
			LUX = {
				add_equipment_to_stockpile = { type = infantry_equipment_1 amount = 16000 }
				add_equipment_to_stockpile = { type = support_equipment_1 amount = 900 }
				add_equipment_to_stockpile = {
					type = train_equipment_1
					amount = 60
				}
			}
            GRE = { #ANTI VMRO
				add_equipment_to_stockpile = { type = infantry_equipment_0 amount = 4000 }
			}
			YUG = { #ANTI VMRO
				add_equipment_to_stockpile = { type = infantry_equipment_1 amount = 4000 }
			}

        }
    }
    on_monthly = {
        effect = { HMM_apply_intel = yes }
    }
    on_capitulation_immediate = {
        effect = {
            # JAP delete EQ
            if = {
                limit = { ROOT = { tag = JAP } }
                set_equipment_fraction = 0.00
            }
        }
    }
    on_capitulation = {
        effect = {
            if = {
                limit = { 
                    tag = BEL
                }
                FRA = { transfer_state = 538 }
                FRA = { transfer_state = 295 }
                FRA = { transfer_state = 718 }
                FRA = { transfer_state = 890 }
                FRA = { transfer_state = 888 }
                FRA = { transfer_state = 889 }
                FRA = { transfer_state = 768 }
                FRA = { transfer_state = 769 }

                538 = { set_compliance = 100 }
                295 = { set_compliance = 100 }
                718 = { set_compliance = 100 }
                890 = { set_compliance = 100 }
                888 = { set_compliance = 100 }
                889 = { set_compliance = 100 }
                768 = { set_compliance = 100 }
                769 = { set_compliance = 100 }
            }
            if = {
                limit = { tag = HOL }
                ENG = { transfer_state = 309 }
                309 = { set_compliance = 100 }
                ENG = { transfer_state = 695 }
                695 = { set_compliance = 100 }
            }
            if = {
                limit = {
                    tag = POL
                }
                SOV = {
                    transfer_state = 96
                    transfer_state = 784
                    transfer_state = 95
                    transfer_state = 94
                    transfer_state = 93
                    transfer_state = 91
                    transfer_state = 89
                    transfer_state = 97
                }
            }
            if = {
                limit = {
                    is_allies_member_ai = yes
                }
                delete_unit_template_and_units = {
                    division_template = "Inf Division"
                    disband = no
                }
            }
        }
        #   ███████╗ ██████╗██████╗ ██╗██████╗ ████████╗███████╗██████╗     ██████╗ ███████╗ █████╗  ██████╗███████╗
        #   ██╔════╝██╔════╝██╔══██╗██║██╔══██╗╚══██╔══╝██╔════╝██╔══██╗    ██╔══██╗██╔════╝██╔══██╗██╔════╝██╔════╝
        #   ███████╗██║     ██████╔╝██║██████╔╝   ██║   █████╗  ██║  ██║    ██████╔╝█████╗  ███████║██║     █████╗  
        #   ╚════██║██║     ██╔══██╗██║██╔═══╝    ██║   ██╔══╝  ██║  ██║    ██╔═══╝ ██╔══╝  ██╔══██║██║     ██╔══╝  
        #   ███████║╚██████╗██║  ██║██║██║        ██║   ███████╗██████╔╝    ██║     ███████╗██║  ██║╚██████╗███████╗
        #   ╚══════╝ ╚═════╝╚═╝  ╚═╝╚═╝╚═╝        ╚═╝   ╚══════╝╚═════╝     ╚═╝     ╚══════╝╚═╝  ╚═╝ ╚═════╝╚══════╝
        effect = {
            # China
            if = {
                limit = { 
                    original_tag = CHI
                    JAP = { has_war_with = CHI }
                } 
                
                JAP = {
                    add_named_threat = { 
                        threat = 11.9
                        name = "China Deadge"
                    }
                    annex_country = {
                        target = CHI
                        transfer_troops = no
                    }
                }
            }
            # JAP
            if = {
                limit = { 
                    original_tag = JAP
                    USA = { has_war_with = JAP }
                } 
                
                USA = {
                    add_named_threat = { 
                        threat = 187
                        name = "Japan died of Cringe"
                    }
                    annex_country = {
                        target = JAP
                        transfer_troops = no
                    }
                }
            }
        }
    }
    on_peace = {
        effect = {
            THIS = { add_ideas = peacetime_training }        
        }
    }
}
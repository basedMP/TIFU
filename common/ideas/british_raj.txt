ideas = {

	hidden_ideas = {
		RAJ_lions_of_the_great_war = {

			allowed = {
				original_tag = RAJ
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			modifier = {
				max_command_power = 25
				dig_in_speed_factor = 0.5
			}
		}

		RAJ_indian_gurkhas = {

			allowed = {
				original_tag = RAJ
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			modifier = {
				conscription_factor = 0.03
			}
		}
	}

	country = {

		RAJ_fix_the_food_distribution = {
			picture = raj_risk_of_famine

			allowed = {
				original_tag = RAJ
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			modifier = {
				civilian_factory_use = 5
			}
		}
	
		RAJ_azad_hind = {
			picture = generic_morale_bonus

			allowed = {
				original_tag = RAJ
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			modifier = {
				fascism_drift = 0.05
			}
		}

		RAJ_marginalized_muslim_community = {
		
			picture = raj_marginalized_muslim_community

			allowed = {
				original_tag = RAJ
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			modifier = {
				stability_factor = -0.1
			}
		}

		RAJ_marginalized_muslim_community_angry = {
		
			picture = raj_marginalized_muslim_community_angry

			allowed = {
				original_tag = RAJ
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			modifier = {
			}
		}

		RAJ_marginalized_muslim_community_happy = {
		
			picture = raj_marginalized_muslim_community_happy

			allowed = {
				original_tag = RAJ
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			modifier = {
				army_core_attack_factor = 0.05
				army_core_defence_factor = 0.05
			}
		}

		RAJ_risk_of_famine = {

			picture = raj_risk_of_famine

			allowed = {
				original_tag = RAJ
			}

			allowed_civil_war = {
				NOT = {
					OR = {
						has_government = communism
						has_government = fascism
					}
				}
			}

			removal_cost = -1

			modifier = {

			}
		}

		RAJ_cap_idea = {
			name = "Improved Worker Conditions"
			picture = generic_production_bonus

			allowed = {
				original_tag = RAJ
			}

			removal_cost = -1

			modifier = {
				production_factory_max_efficiency_factor = 0.1
			}
		}

		RAJ_construction_idea = {
			name = "Indian Construction Company"
			picture = generic_production_bonus

			allowed = {
				original_tag = RAJ
			}

			removal_cost = -1

			modifier = {
				production_speed_buildings_factor = 0.1
			}
		}

		RAJ_princely_states = {

			picture = generic_morale_bonus

			allowed = {
				original_tag = RAJ
			}

			allowed_civil_war = {
				NOT = {
					OR = {
						has_government = communism
						has_government = fascism
					}
				}
			}

			removal_cost = -1

			modifier = {
				industrial_capacity_factory = -0.5
			}
		}

		RAJ_princely_states_election = {

			picture = generic_morale_bonus

			allowed = {
				original_tag = RAJ
			}

			allowed_civil_war = {
				NOT = {
					OR = {
						has_government = communism
						has_government = fascism
					}
				}
			}

			removal_cost = -1

			modifier = {
				autonomy_gain = -0.4
			}
		}

		RAJ_princely_states_donations = {

			picture = generic_production_bonus

			allowed = {
				original_tag = RAJ
			}

			allowed_civil_war = {
				NOT = {
					OR = {
						has_government = communism
						has_government = fascism
					}
				}
			}

			removal_cost = -1

			modifier = {
				industrial_capacity_factory = -0.25
			}
		}

		RAJ_princely_states_donations2 = {
			name = "Princely States"
			picture = generic_production_bonus

			allowed = {
				original_tag = RAJ
			}

			allowed_civil_war = {
				NOT = {
					OR = {
						has_government = communism
						has_government = fascism
					}
				}
			}

			removal_cost = -1

			modifier = {
				consumer_goods_factor = -0.1
				industrial_capacity_factory = -0.1
			}
		}

		RAJ_princely_states_donations_troops = {

			picture = generic_manpower_bonus

			allowed = {
				original_tag = RAJ
			}

			allowed_civil_war = {
				NOT = {
					OR = {
						has_government = communism
						has_government = fascism
					}
				}
			}

			removal_cost = -1

			modifier = {
				autonomy_gain = -0.4
				consumer_goods_factor = -0.1
				conscription_factor = 0.05
			}
		}

		RAJ_indian_gentlemen_officers = {

			picture = raj_indian_gentlemen_officers

			allowed = {
				original_tag = RAJ
			}

			allowed_civil_war = {
				NOT = {
					OR = {
						has_government = communism
						has_government = fascism
					}
				}
			}

			removal_cost = -1

			modifier = {
				military_leader_cost_factor = -0.50
				army_leader_start_level = 1
			}
		}

		RAJ_agrarian_society = {

			picture = generic_agrarian_society

			allowed = {
				original_tag = RAJ
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			modifier = {
				conscription_factor = -0.5
			}
		}

		RAJ_quit_india_movement = {
		
			picture = raj_quit_india_movement
			
			allowed = {
				original_tag = RAJ
			}

			allowed_civil_war = {
				always = no
			}
			
			removal_cost = -1
			
			modifier = {
				min_export = -1
				autonomy_gain = 0.5
			}
		}
	}


	political_advisor = {

		RAJ_bhimrao_ramji_ambedkar = {
			
			allowed = {
				original_tag = RAJ
			}
			
			picture = generic_political_advisor_india_1
			
			traits = { war_industrialist }
		
			
		
			ai_will_do = {
				factor = 0
			}
		}


		RAJ_karam_singh_mann = {

			allowed = {
				original_tag = RAJ
			}

			available = {

			}

			picture = generic_political_advisor_india_2
			
			traits = { democratic_reformer }			
			
			do_effect = {
				NOT = {
					has_government = communism
				}
			}
			
			ai_will_do = {
				factor = 0
			}

		}


		RAJ_john_edward_golightly = {

			picture = generic_political_advisor_europe_5
			
			allowed = {
				original_tag = RAJ
			}
			
			traits = { silent_workhorse }
		}

		

		RAJ_mahatma_mohandas_gandhi = {

			picture = generic_political_advisor_arab_3
			
			allowed = {
				original_tag = RAJ
			}
			
			traits = { popular_figurehead }
		}

		RAJ_john_mathai = {

			picture = generic_political_advisor_europe_1
			
			allowed = {
				original_tag = RAJ
			}
			
			traits = { captain_of_industry }
		}

		
	}

	naval_manufacturer = {
		
		designer = yes
		
		mazagon_dock_limited = {
			
			allowed = {
				has_dlc = "Together for Victory"
				original_TAG = RAJ
			}

			picture = generic_naval_manufacturer_1
						
			research_bonus = {
				naval_equipment = 0.15
			}
			
			traits = { convoy_escort_naval_manufacturer }

		}
		
		garden_reach_shipbuilders = {
			
			allowed = {
				has_dlc = "Together for Victory"
				original_TAG = RAJ
			}
			
			picture = generic_naval_manufacturer_3
			
			research_bonus = {
				naval_equipment = 0.15
			}
			
			traits = { pacific_fleet_naval_manufacturer }
		}
		
		scindia_shipyard = {
			
			allowed = {
				has_dlc = "Together for Victory"
				original_TAG = RAJ
			}
			
			picture = generic_naval_manufacturer_2
			
			research_bonus = {
				naval_equipment = 0.15
			}
			
			traits = { atlantic_fleet_naval_manufacturer }
			
		}
		
	}

	aircraft_manufacturer = {
			
		designer = yes
		
		hindustan_aircraft= {
			
			allowed = {
				has_dlc = "Together for Victory"
				original_TAG = RAJ
			}
			
			picture = generic_air_manufacturer_1
			
			research_bonus = {
				air_equipment = 0.15
			}
			
			traits = { light_aircraft_manufacturer }
			
			modifier = {
			}
		}
					
	}
	materiel_manufacturer = {
			
		designer = yes
		
		ishapore_rifle_factory = {
			
			allowed = {
				has_dlc = "Together for Victory"
				original_TAG = RAJ
			}

			picture = generic_infantry_equipment_manufacturer_2
			
			research_bonus = {
				infantry_weapons = 0.15
			}
			
			traits = { infantry_equipment_manufacturer }
			
			modifier = {
			}
		}
		
		ordnance_factories_board = {
			
			allowed = {
				has_dlc = "Together for Victory"
				original_TAG = RAJ
			}
			
			picture = generic_artillery_manufacturer_2
			
			research_bonus = {
				artillery = 0.15
			}
			
			traits = { artillery_manufacturer }
			
			modifier = {
			}
		}
	}
}
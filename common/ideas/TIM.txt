ideas = {

	country = {
		HMM_JAP_spymaster = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = generic_spy_political
			
			modifier = {
				operative_slot = 1
			}
		}
		ROM_compensation = {
			name = "Compensation for lost Territories"
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = GFX_focus_generic_africa_factory
			
			modifier = {
                industrial_capacity_factory = 0.15
            }
		}
		ROM_exploit_the_baita_mines_T = {
			name = "Exploit The Baita Mines"
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = generic_exploit_mines
			
			modifier = {
                local_resources_factor = 0.2
            }
		}
		ROM_royal_guards_divisions = {
			name = "Royal Guards Divisions"
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = general_staff
			
			modifier = {
                army_attack_factor = 0.05
            }
		}
		ROM_makeshift_dday_defense = {
			name = "Makeshift D-Day Defense"
			picture = generic_fortify_the_borders

			removal_cost = -1
			
			allowed = {
				original_tag = ROM
			}

			allowed_civil_war = {

			}

			modifier = {
				army_defence_factor = 0.05
				dig_in_speed_factor = 0.2
				army_morale_factor = 0.1
				army_speed_factor = 0.1
			}
		}
		TIM_victory_in_africa = {
			name = "Victory in Afrika"
			picture = generic_fortify_the_borders

			removal_cost = -1
			
			allowed = {
				OR = {
					AND = {
						owns_state = 454
						owns_state = 907
					}
					any_allied_country = {
						owns_state = 454
						owns_state = 907
					}
				}
			}
			cancel = {
				NOT = {
					OR = {
						AND = {
							owns_state = 454
							owns_state = 907
						}
						any_allied_country = {
							owns_state = 454
							owns_state = 907
						}
					}
				}
			}

			allowed_civil_war = {

			}

			modifier = {
				industrial_capacity_factory = 0.10
			}
		}
		ROM_escalate_the_war = {
			name = "Escalate the War"
			picture = GFX_focus_SOV_the_glory_of_the_red_army_communism

			removal_cost = -1
			
			allowed = {
			}

			allowed_civil_war = {

			}

			targeted_modifier = {
				tag = SOV
				attack_bonus_against = 0.1
				defense_bonus_against = 0.1
			}
		}
		GER_Flak = {
			picture = generic_artillery_regiments
			name = "8,8cm Flug- & Panzerabwehrkanone"
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			

			equipment_bonus = {
				anti_air_equipment = {
					air_attack = 0.05
					hard_attack = 0.1
					build_cost_ic = 0.15
					instant = yes
				}
			}		
		}
		GER_v2 = {
			picture = generic_artillery_regiments
			name = "Vergeltungswaffe 2"
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			

			equipment_bonus = {
				rocket_artillery_equipment = {
					soft_attack = 0.1
					build_cost_ic = 0.15
					instant = yes
				}
			}
		}
		GER_wissenschaften = {
			picture = GFX_GER_Deutsche_Arbeitsfront
			name = "Überlegene Wissenschaften"
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			modifier = {
				research_speed_factor = 0.10
			}
		}
		GER_endsieg = {
			picture = GFX_endsieg
			name = "Endsieg"
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			modifier = {
				army_core_attack_factor = 0.15
				army_core_defence_factor = 0.15
				stability_weekly = -0.005
				war_support_weekly = -0.005
				production_speed_buildings_factor = -0.2
				industrial_capacity_factory = -0.2
				industrial_capacity_dockyard = -0.2
			}
		}
		no_tradeback = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				OR = {
					GER = { has_completed_focus = GER_danzig_or_war }
					GER = { has_war = yes }
				}
			}

			removal_cost = -1

			picture = generic_flexible_foreign_policy #FRA_market_dynamism #generic_political_support #generic_build_infrastructure

			modifier = {
				production_lack_of_resource_penalty_factor = -1
				min_export = -1
				air_fuel_consumption_factor = -0.99
				navy_fuel_consumption_factor = -0.99
			}
		}

		peacetime_training = {
			allowed = {
				always = no
			}
			cancel = {
				has_war = yes
			}
			removal_cost = -1
			picture = generic_infantry_bonus
			modifier = {
				training_time_army_factor = -0.95
				air_accidents_factor = -1.25
				naval_accidents_chance = -1
				air_fuel_consumption_factor = -0.99
				navy_fuel_consumption_factor = -0.99
				mobilization_speed = 2.0
			}
		}
		
		HMM_vol_major = {
			picture = generic_artillery_regiments
			name = "Major Volns"
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			

			modifier = {
				air_volunteer_cap = 3
				send_volunteer_size = 5
			}
		}
		HMM_vol_minor = {
			picture = generic_artillery_regiments
			name = "Minor Volns"
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			

			modifier = {
				send_volunteer_size = 2
			}
		}
		HMM_vol_vol = {
			picture = generic_artillery_regiments
			name = "Volunteernation"
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			

			modifier = {
				send_volunteer_size = 187
			}
		}
		HMM_dont_produce = { # ai_limiter
			allowed = {
				always = no
			}
			name = "AI"

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = generic_production_bonus

			modifier = {
				production_factory_max_efficiency_factor = -1
                production_factory_start_efficiency_factor = -1
                production_factory_efficiency_gain_factor = -1
                consumer_goods_expected_value = 2
				political_power_gain = -5
				training_time_factor = 1000
			}
		}

		FRA_industrialisation_of_africa = {
			allowed = {
				always = no
			}
			name = "Industrialization of Africa"

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = generic_production_bonus

			modifier = {
                production_factory_efficiency_gain_factor = 0.2
                production_factory_start_efficiency_factor = 0.2
			}
		}

		FRA_africa_1 = {
			picture = generic_central_management
			name = "Industrialization of Africa: Very Low"
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			

			equipment_bonus = {
				light_tank_chassis = {
					instant = yes
					build_cost_ic = -0.02
				}
				medium_tank_chassis = {
					instant = yes
					build_cost_ic = -0.02
				}
				heavy_tank_chassis = {
					instant = yes
					build_cost_ic = -0.02
				}
			}
		}

		FRA_africa_2 = {
			picture = generic_central_management
			name = "Industrialization of Africa: Low"
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			

			equipment_bonus = {
				light_tank_chassis = {
					instant = yes
					build_cost_ic = -0.04
				}
				medium_tank_chassis = {
					instant = yes
					build_cost_ic = -0.04
				}
				heavy_tank_chassis = {
					instant = yes
					build_cost_ic = -0.04
				}
			}
		}

		FRA_africa_3 = {
			picture = generic_central_management
			name = "Industrialization of Africa: Medium"
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			

			equipment_bonus = {
				light_tank_chassis = {
					instant = yes
					build_cost_ic = -0.06
				}
				medium_tank_chassis = {
					instant = yes
					build_cost_ic = -0.06
				}
				heavy_tank_chassis = {
					instant = yes
					build_cost_ic = -0.06
				}
			}
		}
		FRA_africa_4 = {
			picture = generic_central_management
			name = "Industrialization of Africa: High"
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			

			equipment_bonus = {
				light_tank_chassis = {
					instant = yes
					build_cost_ic = -0.08
				}
				medium_tank_chassis = {
					instant = yes
					build_cost_ic = -0.08
				}
				heavy_tank_chassis = {
					instant = yes
					build_cost_ic = -0.08
				}
			}
		}
		FRA_africa_5 = {
			picture = generic_central_management
			name = "Industrialization of Africa: Maximized"
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			

			equipment_bonus = {
				light_tank_chassis = {
					instant = yes
					build_cost_ic = -0.10
				}
				medium_tank_chassis = {
					instant = yes
					build_cost_ic = -0.10
				}
				heavy_tank_chassis = {
					instant = yes
					build_cost_ic = -0.10
				}
			}
		}
		FRA_prepare = {
			picture = generic_central_management
			name = "Prepare for the German Invasion"
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			

			modifier = {
				industrial_capacity_factory = 0.25
				production_factory_efficiency_gain_factor = 2.0
			}
			equipment_bonus = {
				light_tank_chassis = {
					instant = yes
					build_cost_ic = -0.10
				}
				medium_tank_chassis = {
					instant = yes
					build_cost_ic = -0.10
				}
				heavy_tank_chassis = {
					instant = yes
					build_cost_ic = -0.10
				}
			}
		}
		ALLIES_dday = {
			name = "Death to the Nazi Order"
			picture = SPR_denounce_nazism
			allowed = {
				always = no
			}
			targeted_modifier = {
				tag = GER
				attack_bonus_against = 0.3
				defense_bonus_against = 0.3
			}
			targeted_modifier = {
				tag = ITA
				attack_bonus_against = 0.3
				defense_bonus_against = 0.3
			}
			targeted_modifier = {
				tag = HUN
				attack_bonus_against = 0.3
				defense_bonus_against = 0.3
			}
			targeted_modifier = {
				tag = ROM
				attack_bonus_against = 0.3
				defense_bonus_against = 0.3
			}
			targeted_modifier = {
				tag = BUL
				attack_bonus_against = 0.3
				defense_bonus_against = 0.3
			}
			targeted_modifier = {
				tag = SPR
				attack_bonus_against = 0.3
				defense_bonus_against = 0.3
			}
			targeted_modifier = {
				tag = FIN
				attack_bonus_against = 0.3
				defense_bonus_against = 0.3
			}
		}
		ITA_imperium_romanum_idea = {
			name = "Imperium Romanum"
			picture = SOV_third_five_year_plan_military
			
			allowed = {
				original_tag = "ITA"
			}

			available = {
				
			}

			modifier = {
				stability_factor = 0.1
				industrial_capacity_factory = 0.1
				industrial_capacity_dockyard = 0.1
			}
		}
		ITA_mare_nostrum = {
			name = ITA_mare_nostrum_b
			picture = generic_navy_bonus
			
			allowed = {
				original_tag = "ITA"
			}

			available = {
				
			}

			modifier = {
				industrial_capacity_dockyard = 0.15
			}
		}
		TIFU_attache = {
			name = "Attache Sent"
			picture = received_attache
			
			allowed = { }

			available = {
				
			}

			modifier = {
				war_support_factor = 0.1
				experience_gain_army = 0.15
				experience_gain_air = 0.1
			}
		}
		ROM_greater_romania = {
			picture = SPR_egypt_1
			
			allowed = {
				original_tag = ROM
			}

			available = {
				
			}

			modifier = {
				stability_factor = 0.1
				operation_steal_tech_risk = -1.0
				legitimacy_gain_factor = 1.0
			}
		}
		JAP_strike_south_preparations = {
			picture = GFX_focus_jap_strike_south
			
			allowed = {
				original_tag = "JAP"
			}
			cancel = {
				has_war_with = USA
			}

			available = {
				
			}

			modifier = {
				industrial_capacity_dockyard = 0.15
			}
		}
		USA_ic_navy = {
			picture = GFX_focus_generic_navy_battleship2
			name = "Sign the Metherell-Act"
			
			allowed = {
				original_tag = "USA"
			}

			available = {
				
			}

			modifier = {
				industrial_capacity_dockyard = 0.15
			}
		}
		USA_ic_arms = {
			picture = GFX_focus_generic_navy_battleship2
			name = "Nationwide Crab Rave Clubs"
			
			allowed = {
				original_tag = "USA"
			}

			available = {
				
			}

			modifier = {
				industrial_capacity_factory = 0.15
			}
		}
		JAP_no_china_war = {
			picture = GFX_focus_generic_navy_battleship2
			name = "Exhausted Sino-Japanese Relations"
			
			allowed = {
				original_tag = "JAP"
			}

			cancel = {
				has_war_with = CHI
			}

			modifier = {
				industrial_capacity_factory = -0.10
			}
		}
		JAP_timed_schlot = {
			picture = GFX_focus_generic_navy_battleship2
			name = "Unified Fighter Development"
			
			allowed = {
				original_tag = "JAP"
			}

			cancel = {
				has_war_with = CHI
			}

			modifier = {
				research_speed_factor = 0.10
			}
		}
		SPA_lessons_from_scw = {
			picture = GFX_focus_generic_navy_battleship2
			name = "Lessons from the Civil War"
			
			allowed = {
				always = no
			}

			picture = idea_por_iberian_workers_united
			
			modifier = {
				max_planning = 0.1
			}
		}
		SPR_revenge = {
			picture = GFX_focus_generic_navy_battleship2
			name = "Death to all Stone-Stealing Nations (and their followers)"
			
			allowed = {
				always = no
			}

			picture = idea_por_iberian_workers_united
			
			targeted_modifier = {
				tag = ENG
				attack_bonus_against = 0.1
				defense_bonus_against = 0.1
			}
			targeted_modifier = {
				tag = AST
				attack_bonus_against = 0.1
				defense_bonus_against = 0.1
			}
			targeted_modifier = {
				tag = RAJ
				attack_bonus_against = 0.1
				defense_bonus_against = 0.1
			}
			targeted_modifier = {
				tag = FRA
				attack_bonus_against = 0.1
				defense_bonus_against = 0.1
			}
		}
		SPA_italy_program = {
			name = "The Italy Program"
			
			allowed = {
				always = no
			}

			picture = idea_por_iberian_workers_united
			
			modifier = {
				production_factory_efficiency_gain_factor = 0.25
			}
		}
		SPA_germany_program = {
			name = "The Germany Program"
			
			allowed = {
				always = no
			}

			picture = idea_por_iberian_workers_united
			
			modifier = {
				production_factory_max_efficiency_factor = 0.05
				production_factory_efficiency_gain_factor = -0.05
			}
		}
		TIFU_minor = {
			name = "Minor"
			
			allowed = {
				always = no
			}

			picture = idea_por_iberian_workers_united
			
			modifier = {
				air_range_factor = -1
			}
		}
		TIFU_major = {
			name = "Major"
			
			allowed = {
				always = no
			}

			picture = idea_por_iberian_workers_united
			
			modifier = {
				air_range_factor = 1
			}
		}
    }
}
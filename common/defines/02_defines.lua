NDefines.NGame.HANDS_OFF_START_TAG = "BKB"

--zero xp
NDefines.NProduction.EQUIPMENT_MODULE_ADD_XP_COST = 0
NDefines.NProduction.EQUIPMENT_MODULE_REPLACE_XP_COST = 0
NDefines.NProduction.EQUIPMENT_MODULE_CONVERT_XP_COST = 0
NDefines.NProduction.EQUIPMENT_MODULE_REMOVE_XP_COST = 0
NDefines.NMilitary.BASE_DIVISION_BRIGADE_GROUP_COST = 0 --
NDefines.NMilitary.BASE_DIVISION_BRIGADE_CHANGE_COST = 0
NDefines.NMilitary.BASE_DIVISION_SUPPORT_SLOT_COST = 0
NDefines.NMilitary.MAX_ARMY_EXPERIENCE = 9999 --
NDefines.NMilitary.MAX_NAVY_EXPERIENCE = 9999
NDefines.NMilitary.MAX_AIR_EXPERIENCE = 9999

--diOr gameplay

--AI
NDefines.NAI.DIPLOMACY_ACCEPT_ATTACHE_BASE = 150
NDefines.NAI.DAYS_BETWEEN_CHECK_BEST_DOCTRINE = 7000       -- Recalculate desired best doctrine to unlock with this many days inbetween.
NDefines.NAI.DAYS_BETWEEN_CHECK_BEST_TEMPLATE = 7000      -- Recalculate desired best template to upgrade with this many days inbetween.
NDefines.NAI.DAYS_BETWEEN_CHECK_BEST_EQUIPMENT = 7000      -- Recalculate desired best equipment to upgrade with this many days inbetween.
NDefines.NAI.HOURS_BETWEEN_ENCIRCLEMENT_DISCOVERY = 700001	-- Per army, interval in hours between refresh of which provinces it considers make up potential encirclement points
NDefines.NAI.DIPLOMACY_REJECTED_WAIT_MONTHS_BASE = 24                --up from 4 | should cut down on AI spam
NDefines.NAI.DIPLOMACY_SEND_EXPEDITIONARY_BASE = 0

-- air
NDefines.NAir.AIR_WING_FLIGHT_SPEED_MULT = 0.04 --0.02 --faster redeployment between asia/europe					-- Global speed multiplier for airplanes (affects fe.transferring to another base)
NDefines.NAir.AIR_DEPLOYMENT_DAYS = 0                              -- Down from 3
NDefines.NAir.ACE_WING_SIZE_MAX_BONUS = 1                       -- biggest bonus we can get from having a small wing with an ace on
NDefines.NAir.MAX_FUEL_FLOW_MULT = 4.0 --1 -- max fuel flow ratio for planes which will be multiplied by supply
NDefines.NAir.FUEL_COST_MULT = 1.4 --0.35 -- fuel multiplier for all air missions
NDefines.NAir.MIN_PLANE_COUNT_PARADROP = 10000 -- 50

NDefines.NAir.NAVAL_STRIKE_TARGETTING_TO_AMOUNT = 0.4	--0.3	Balancing value to convert the naval_strike_targetting equipment stats to chances of how many airplanes managed to do successfull strike.	
NDefines.NAir.NAVAL_STRIKE_DAMAGE_TO_STR = 1.0	--1.0	Balancing value to convert damage ( naval_strike_attack * hits ) to Strength reduction.	
NDefines.NAir.NAVAL_STRIKE_DAMAGE_TO_ORG = 1.5 --1.5	Balancing value to convert damage ( naval_strike_attack * hits ) to Organisation reduction.	
NDefines.NAir.NAVAL_STRIKE_CARRIER_MULTIPLIER = 10 --10.0	damage bonus when planes are in naval combat where their carrier is present (and can thus sortie faster and more effectively)

NDefines.NAir.AIR_WING_BOMB_DAMAGE_FACTOR = 8 --2					-- Used to balance the damage done while bombing.
NDefines.NAir.COMBAT_DAMAGE_STATS_MULTILPIER = 0.2
NDefines.NAir.COMBAT_DAMAGE_SCALE = 0.25 --1							-- Higher value = more shot down planes
NDefines.NAir.COMBAT_DAMAGE_SCALE_CARRIER = 5 --5					-- same as above but used inside naval combat for carrier battles
NDefines.NAir.DETECT_CHANCE_FROM_AIRCRAFTS_EFFECTIVE_COUNT = 750 --3000 -- Max amount of aircrafts in region to give full detection bonus. 1/4th cause 1/4th of planes
NDefines.NAir.FIELD_EXPERIENCE_FACTOR = 1.4 --0.7						-- Factor all air experience gain from missions by this
	
NDefines.NAir.ACCIDENT_CHANCE_BALANCE_MULT = 0.25 --1					-- Multiplier for balancing how often the air accident really happens. The higher mult the more often.
NDefines.NAir.AIR_AGILITY_TO_NAVAL_STRIKE_AGILITY = 0.01         		-- conversion factor to bring agility in line with ship AA
NDefines.NAir.AIR_DAMAGE_TO_DIVISION_LOSSES = 1.0				-- factor for conversion air damage to division losses for details statistics of air wings
NDefines.NAir.AIR_NAVAL_KAMIKAZE_DAMAGE_MULT = 20.0				-- Balancing value to increase usual damage to Strength for Kamikaze
NDefines.NAir.AIR_NAVAL_KAMIKAZE_LOSSES_MULT = 4.0          			-- Balancing value to increase usual losses if Kamikaze participating in the battle
NDefines.NAir.BASE_STRATEGIC_BOMBING_HIT_SHIP_CHANCE = 0.01		-- Chance to hit a ship in port when it is bombed.
NDefines.NAir.BASE_STRATEGIC_BOMBING_HIT_SHIP_DAMAGE_FACTOR = 50
NDefines.NAir.BASE_STRATEGIC_BOMBING_HIT_PLANE_CHANCE = 0.1 --0.5		-- Chance to hit a plane in airbase when it is bombed. bs modifier
NDefines.NAir.BASE_STRATEGIC_BOMBING_HIT_PLANE_DAMAGE_FACTOR = 0.2

NDefines.NAir.AA_INDUSTRY_AIR_DAMAGE_FACTOR = -0.12				-- 5x levels = 60% defense from bombing
NDefines.NAir.ANTI_AIR_PLANE_DAMAGE_FACTOR = 0.2--0.8					-- Anti Air Gun Damage factor
NDefines.NAir.ANTI_AIR_ATTACK_TO_DAMAGE_REDUCTION_FACTOR = 1.0	-- Balancing value to convert equipment stat anti_air_attack to the damage reduction modifier apply to incoming air attacks against units with AA.
NDefines.NAir.ANTI_AIR_MAXIMUM_DAMAGE_REDUCTION_FACTOR = 0.75	-- Maximum damage reduction factor applied to incoming air attacks against units with AA.
NDefines.NAir.NAVAL_STRIKE_BASE_STR_TO_PLANES_RATIO = 0.03		-- Max airbombers to do port strike comparing to strength

NDefines.NMilitary.LAND_AIR_COMBAT_STR_DAMAGE_MODIFIER = 0.1	--0.032	--air global damage modifier	
NDefines.NMilitary.LAND_AIR_COMBAT_ORG_DAMAGE_MODIFIER = 0.1	--0.032	--global damage modifier	
NDefines.NMilitary.LAND_AIR_COMBAT_MAX_PLANES_PER_ENEMY_WIDTH = 1 --3 

----logibombing?
--AIR_WING_ATTACK_LOGISTICS_NO_TRUCK_DISRUPTION_FACTOR = 0.02 -- If a unit isn't motorized still disrupt its supply by damage * this
--AIR_WING_ATTACK_LOGISTICS_TRUCK_DAMAGE_FACTOR = 0.27
--AIR_WING_ATTACK_LOGISTICS_INFRA_DAMAGE_SPILL_FACTOR = 0.0016 -- Portion of truck damage to additionally deal to infrastructure
--AIR_WING_ATTACK_LOGISTICS_TRAIN_DAMAGE_FACTOR = 0.040
--AIR_WING_ATTACK_LOGISTICS_TRAIN_DAMAGE_DISRUPTION_MITIGATION = 6.0 -- Multiply Train Damage by (Smooth / (Smooth + (Disruption * Mitigation)))
--AIR_WING_ATTACK_LOGISTICS_TRAIN_DAMAGE_DISRUPTION_SMOOTHING = 5.0
--AIR_WING_ATTACK_LOGISTICS_RAILWAY_DAMAGE_SPILL_FACTOR = 0.006 -- Portion of train damage to additionally deal to railways
--
--AIR_WING_ATTACK_LOGISTICS_DISRUPTION_MIN_DAMAGE_FACTOR = 0.1 -- Multiply train damage by this factor scale from 1.0 at 0 disruption to this at AIR_WING_ATTACK_LOGISTICS_MAX_DISRUPTION_DAMAGE_TO_CONSIDER
--AIR_WING_ATTACK_LOGISTICS_MAX_DISRUPTION_DAMAGE_TO_CONSIDER = 15.0 -- see above
--AIR_WING_ATTACK_LOGISTICS_DIRECT_DISRUPTION_DAMAGE_FACTOR = 0.01 -- Disruption damage to supply throughput done by bombing damage not dependant on killing trains which also causes diruption.
--
--AIR_WING_ATTACK_LOGISTICS_TRUCK_MAX_FACTOR = 0.3 -- max trucks we can destroy in one instance of a logistics strike
--SECONDARY_DAMAGE_STRAT = 0.2  -- how much damage gets translated to railway guns for strat bombing
--SECONDARY_DAMAGE_LOGISTICS = 1.0 -- how much damage gets translated to railway guns for logistic strike

-- building
NDefines.NBuildings.OWNER_CHANGE_EXTRA_SHARED_SLOTS_FACTOR = 1.0 -- You get all the factories in a territory when you annex it

NDefines.NBuildings.MAX_BUILDING_LEVELS = 300 --schlotikus
NDefines.NBuildings.MAX_SHARED_SLOTS = 300 --schlotikus

--- country
NDefines.NCountry.SPECIAL_FORCES_CAP_MIN = 672 -- 2x24 14-4
NDefines.NCountry.SPECIAL_FORCES_CAP_BASE = 0.00

--diplo
NDefines.NDiplomacy.PEACE_SCORE_PER_PASS = 100000.0
NDefines.NDiplomacy.VOLUNTEERS_PER_TARGET_PROVINCE = 0--0.05			-- Each province owned by the target country contributes this amount of volunteers to the limit.
NDefines.NDiplomacy.VOLUNTEERS_PER_COUNTRY_ARMY = 0--0.05				-- Each army unit owned by the source country contributes this amount of volunteers to the limit.
NDefines.NDiplomacy.VOLUNTEERS_RETURN_EQUIPMENT = 1--0.95				-- Returning volunteers keep this much equipment
NDefines.NDiplomacy.VOLUNTEERS_TRANSFER_SPEED = 7 --14					-- days to transfer a unit to another nation
NDefines.NDiplomacy.VOLUNTEERS_DIVISIONS_REQUIRED = 0--30
NDefines.NDiplomacy.BASE_SEND_ATTACHE_COST = 0 --100, free watching
NDefines.NDiplomacy.BASE_SEND_ATTACHE_CP_COST = 0 --50, free watching
NDefines.NDiplomacy.ATTACHE_TO_SUBJECT_EFFECT = 0 --no autonomy gain
NDefines.NDiplomacy.ATTACHE_TO_OVERLORD_EFFECT = 0 --no autonomy gain
NDefines.NDiplomacy.ATTACHE_XP_SHARE = 0 --0.15 no xp from attache
-- mil
NDefines.NRailwayGun.ENCIRCLED_DISBAND_MANPOWER_FACTOR = 0		-- The percentage of manpower returned when an encircled unit is disbanded
NDefines.NMilitary.ENCIRCLED_DISBAND_MANPOWER_FACTOR = 0

--NDefines.NMilitary.CORPS_COMMANDER_DIVISIONS_CAP = 72
--NDefines.NMilitary.FIELD_MARSHAL_DIVISIONS_CAP = 72
--NDefines.NMilitary.GARRISON_ORDER_ARMY_CAP_FACTOR = 1.0 --default 72, no 24*3

NDefines.NMilitary.NUKE_MIN_DAMAGE_PERCENT = 0.14					-- Minimum damage from nukes as a percentage of current strength/organisation
NDefines.NMilitary.NUKE_MAX_DAMAGE_PERCENT = 0.15					-- Minimum damage from nukes as a percentage of current strength/organisation
NDefines.NMilitary.NUKE_DELAY_HOURS = 12							-- How many hours does it take for the nuclear drop to happen

NDefines.NMilitary.COMBAT_MINIMUM_TIME = 2 --micro more responsive

NDefines.NCountry.REINFORCEMENT_MANPOWER_DELIVERY_SPEED = 500.0	-- vanilla 10 Modifier for army manpower reinforcement delivery speed (travel time)
NDefines.NCountry.REINFORCEMENT_MANPOWER_CHUNK = 1            -- vanilla 0.1
NDefines.NCountry.EQUIPMENT_UPGRADE_CHUNK_MAX_SIZE = 100			-- vanilla 10  Maximum chunk size of equipment upgrade distribution per update.
NDefines.NCountry.REINFORCEMENT_EQUIPMENT_DELIVERY_SPEED = 0.3     -- vanilla 0.3 
NDefines.NMilitary.REINFORCEMENT_REQUEST_MAX_WAITING_DAYS = 2   -- Every X days the equipment will be sent, regardless if still didn't produced all that has been requested.
NDefines.NMilitary.REINFORCEMENT_REQUEST_DAYS_FREQUENCY = 2	   -- How many days must pass until we may give another reinforcement request

-- prod
NDefines.NProduction.ANNEX_FIELD_EQUIPMENT_RATIO = 0.5	-- WAS 0.25 | Annex decisions should give all troops, but incase I screwed up here is this | How much equipment from deployed divisions will be transferred on annexation
--NDefines.NProduction.BASE_FACTORY_SPEED_NAV = 1.5 --2.5

-- makes navy less shit to micro
NDefines.NProduction.DEFAULT_MAX_NAV_FACTORIES_PER_LINE = 500 
NDefines.NProduction.CONVOY_MAX_NAV_FACTORIES_PER_LINE = 500
NDefines.NProduction.CAPITAL_SHIP_MAX_NAV_FACTORIES_PER_LINE = 500
NDefines.NProduction.MAX_MIL_FACTORIES_PER_LINE = 500

NDefines.NProduction.MIN_POSSIBLE_TRAINING_MANPOWER = 100000000

NDefines.NProduction.BASE_LICENSE_IC_COST = 0
NDefines.NProduction.LICENSE_IC_COST_YEAR_INCREASE = 0

-- navie
NDefines.NNavy.NAVAL_MINES_NAVAL_SUPREMACY_FACTOR = 0
NDefines.NNavy.EXPERIENCE_FACTOR_CONVOY_ATTACK = 0.01

NDefines.NNavy.SUPREMACY_PER_SHIP_PER_MANPOWER = 0.0 -- from 0.05
NDefines.NNavy.SUPREMACY_PER_SHIP_PER_IC = 0.005 --from 0.005
NDefines.NNavy.SUPREMACY_PER_SHIP_BASE = 0.0 --from 0

NDefines.NNavy.BASE_GUN_COOLDOWNS = { -- number of hours for a gun to be ready after shooting
	1.0,	-- big guns 1
	5.0,	-- torpedoes 4
	1.0,	-- small guns 1
}

NDefines.NNavy.NAVAL_MINES_IN_REGION_MAX = 1.0								-- Max number of mines that can be layed by the ships. The value should be hidden from the user, as we present % so it's an abstract value that should be used for balancing.
NDefines.NNavy.NAVAL_MINES_PLANTING_SPEED_MULT = 0						-- Value used to overall balance of the speed of planting naval mines
NDefines.NNavy.NAVAL_MINES_SWEEPING_SPEED_MULT = 1						-- Value used to overall balance of the speed of sweeping naval mines
NDefines.NNavy.NAVAL_MINES_DECAY_AT_PEACE_TIME = 1							-- How fast mines are decaying in peace time. Planting mines in peace time may be exploitable, so it's blocked atm. That's why after war we should decay them too.
NDefines.NNavy.NAVAL_MINES_INTEL_DIFF_FACTOR = 0					-- Better our decryption over enemy encryption will reduce the penalties from the enemy mines in the region. This value is a factor to be used for balancing.
NDefines.NNavy.NAVAL_MINES_NAVAL_SUPREMACY_FACTOR = 0.0	

-- trade
NDefines.NTrade.ANTI_MONOPOLY_TRADE_FACTOR = 0					-- WAS -100 this group reduces the number of opinion/trade factor changes the game tracks| This is added to the factor value when anti-monopoly threshold is exceeded; cucks majors often if the value is vanilla
NDefines.NTrade.PARTY_SUPPORT_TRADE_FACTOR = 0			-- Trade factor bonus at the other side having 100 % party popularity for my party
NDefines.NTrade.ANTI_MONOPOLY_TRADE_FACTOR_THRESHOLD = 0	-- What percentage of resources has to be sold to the buyer for the anti-monopoly factor to take effect
NDefines.NTrade.MAX_MONTH_TRADE_FACTOR = 0				-- This is the maximum bonus that can be gained from time
NDefines.NTrade.DISTANCE_TRADE_FACTOR = 0				-- Trade factor is modified by distance times this
NDefines.NTrade.RELATION_TRADE_FACTOR = 0				-- Trade factor is modified by Opinion value times this

NDefines.NMarket.IC_TO_CIC_FACTOR = 0.0 				--2.0 -- The factor for mapping IC cost to CIC cost. Should be a positive number.
NDefines.NMarket.MAX_CIV_FACTORIES_PER_CONTRACT = 1		--15 -- Max number of factories that can be assigned for paying single contract.
	

--performanz
NDefines.NGame.GAME_SPEED_SECONDS = { 5000.0, 0.3, 0.19, 0.04, 0.0 } -- SPEED 4 IS 0.1 IN VANILLA  game speeds for each level. Must be 5 entries with last one 0 for unbound
NDefines.NGame.LAG_DAYS_FOR_LOWER_SPEED = 480
NDefines.NGame.LAG_DAYS_FOR_PAUSE = 30
NDefines.NGame.COMBAT_LOG_MAX_MONTHS = 10 -- WAS 48 | drastically cuts down on save file sizes after WW2 starts and well into barbarossa
NDefines.NGame.MESSAGE_TIMEOUT_DAYS = 10 -- WAS 60 	| less messages lying around at the top of your screen
NDefines.NGame.MISSION_REMOVE_FROM_INTERFACE_DEFAULT = 3 -- Default days before a mission is removed from the interface after having failed or completed
NDefines.NFocus.MAX_SAVED_FOCUS_PROGRESS = 30

NDefines.NNavy.NAVAL_COMBAT_RESULT_TIMEOUT_YEARS = 1                -- WAS 2 | after that many years, we clear the naval combat results, so they don't get stuck forever in the memory.
NDefines.NNavy.CONVOY_LOSS_HISTORY_TIMEOUT_MONTHS = 1				-- WAS 24 | after this many months remove the history of lost convoys to not bloat savegames and memory since there is no way to see them anyway

NDefines.NCountry.POPULATION_YEARLY_GROWTH_BASE = 0                     -- Removed for game stability/reducing chance of desync
NDefines.NResistance.GARRISON_LOG_MAX_MONTHS = 0   
NDefines.NCountry.COUNTRY_SCORE_MULTIPLIER = 0				-- Weight of the country score.
NDefines.NCountry.ARMY_SCORE_MULTIPLIER = 0					-- Based on number of armies.
NDefines.NCountry.NAVY_SCORE_MULTIPLIER = 0					-- Based on number of navies.
NDefines.NCountry.AIR_SCORE_MULTIPLIER = 0					-- Based on number of planes (which is typically a lot).
NDefines.NCountry.INDUSTRY_SCORE_MULTIPLIER = 0				-- Based on number of factories.
NDefines.NCountry.PROVINCE_SCORE_MULTIPLIER = 0				-- Based on number of controlled provinces.

NDefines_Graphics.NGraphics.MIN_TRAIN_WAGON_COUNT = 1
NDefines_Graphics.NGraphics.MAX_TRAIN_WAGON_COUNT = 1
NDefines_Graphics.NMapMode.RADAR_ROTATION_SPEED = 0.0
NDefines_Graphics.NAirGfx.AIRPLANES_ANIMATION_GLOBAL_SPEED_PER_GAMESPEED = {0.3, 0.3, 0.3, 0.3, 0.3, 0.3}
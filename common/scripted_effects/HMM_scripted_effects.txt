#    ██████╗ ████████╗██╗  ██╗███████╗██████╗ 
#   ██╔═══██╗╚══██╔══╝██║  ██║██╔════╝██╔══██╗
#   ██║   ██║   ██║   ███████║█████╗  ██████╔╝
#   ██║   ██║   ██║   ██╔══██║██╔══╝  ██╔══██╗
#   ╚██████╔╝   ██║   ██║  ██║███████╗██║  ██║
#    ╚═════╝    ╚═╝   ╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝

HMM_apply_intel = {
    every_country = {
        limit = {
            OR = {
                original_tag = GER
                original_tag = ITA
                original_tag = JAP
                original_tag = BUL
                original_tag = HUN
                original_tag = ROM
                original_tag = FIN
                original_tag = SPR
            }
        }
        if = {
            limit = { NOT = { tag = GER }}
            add_operation_token = {
                tag = GER
                token = E_token_civilian
            }
            add_operation_token = {
                tag = GER
                token = E_token_army
            }
            add_operation_token = {
                tag = GER
                token = E_token_navy
            }
            add_operation_token = {
                tag = GER
                token = E_token_airforce
            }
        }
        if = {
            limit = { NOT = { tag = ITA }}
            add_operation_token = {
                tag = ITA
                token = E_token_civilian
            }
            add_operation_token = {
                tag = ITA
                token = E_token_army
            }
            add_operation_token = {
                tag = ITA
                token = E_token_navy
            }
            add_operation_token = {
                tag = ITA
                token = E_token_airforce
            }
        }
        if = {
            limit = { NOT = { tag = BUL }}
            add_operation_token = {
                tag = BUL
                token = E_token_civilian
            }
            add_operation_token = {
                tag = BUL
                token = E_token_army
            }
            add_operation_token = {
                tag = BUL
                token = E_token_navy
            }
            add_operation_token = {
                tag = BUL
                token = E_token_airforce
            }
        }
        if = {
            limit = { NOT = { tag = ROM }}
            add_operation_token = {
                tag = ROM
                token = E_token_civilian
            }
            add_operation_token = {
                tag = ROM
                token = E_token_army
            }
            add_operation_token = {
                tag = ROM
                token = E_token_navy
            }
            add_operation_token = {
                tag = ROM
                token = E_token_airforce
            }
        }
        if = {
            limit = { NOT = { tag = HUN }}
            add_operation_token = {
                tag = HUN
                token = E_token_civilian
            }
            add_operation_token = {
                tag = HUN
                token = E_token_army
            }
            add_operation_token = {
                tag = HUN
                token = E_token_navy
            }
            add_operation_token = {
                tag = HUN
                token = E_token_airforce
            }
        }
        if = {
            limit = { NOT = { tag = JAP }}
            add_operation_token = {
                tag = JAP
                token = E_token_civilian
            }
            add_operation_token = {
                tag = JAP
                token = E_token_army
            }
            add_operation_token = {
                tag = JAP
                token = E_token_navy
            }
            add_operation_token = {
                tag = JAP
                token = E_token_airforce
            }
        }
        if = {
            limit = { NOT = { tag = SPR }}
            add_operation_token = {
                tag = SPR
                token = E_token_civilian
            }
            add_operation_token = {
                tag = SPR
                token = E_token_army
            }
            add_operation_token = {
                tag = SPR
                token = E_token_navy
            }
            add_operation_token = {
                tag = SPR
                token = E_token_airforce
            }
        }
        if = {
            limit = { NOT = { tag = FIN }}
            add_operation_token = {
                tag = FIN
                token = E_token_civilian
            }
            add_operation_token = {
                tag = FIN
                token = E_token_army
            }
            add_operation_token = {
                tag = FIN
                token = E_token_navy
            }
            add_operation_token = {
                tag = FIN
                token = E_token_airforce
            }
        }
    }
    every_country = {
        limit = {
            OR = {
                original_tag = USA
                original_tag = ENG
                original_tag = FRA
                original_tag = RAJ
                original_tag = AST
                original_tag = SOV
            }
        }
        if = {
            limit = { NOT = { tag = ENG }}
            add_operation_token = {
                tag = ENG
                token = E_token_civilian
            }
            add_operation_token = {
                tag = ENG
                token = E_token_army
            }
            add_operation_token = {
                tag = ENG
                token = E_token_navy
            }
            add_operation_token = {
                tag = ENG
                token = E_token_airforce
            }
        }
        if = {
            limit = { NOT = { tag = USA }}
            add_operation_token = {
                tag = USA
                token = E_token_civilian
            }
            add_operation_token = {
                tag = USA
                token = E_token_army
            }
            add_operation_token = {
                tag = USA
                token = E_token_navy
            }
            add_operation_token = {
                tag = USA
                token = E_token_airforce
            }
        }
        if = {
            limit = { NOT = { tag = FRA }}
            add_operation_token = {
                tag = FRA
                token = E_token_civilian
            }
            add_operation_token = {
                tag = FRA
                token = E_token_army
            }
            add_operation_token = {
                tag = FRA
                token = E_token_navy
            }
            add_operation_token = {
                tag = FRA
                token = E_token_airforce
            }
        }
        if = {
            limit = { NOT = { tag = SOV }}
            add_operation_token = {
                tag = SOV
                token = E_token_civilian
            }
            add_operation_token = {
                tag = SOV
                token = E_token_army
            }
            add_operation_token = {
                tag = SOV
                token = E_token_navy
            }
            add_operation_token = {
                tag = SOV
                token = E_token_airforce
            }
        }
        if = {
            limit = { NOT = { tag = RAJ }}
            add_operation_token = {
                tag = RAJ
                token = E_token_civilian
            }
            add_operation_token = {
                tag = RAJ
                token = E_token_army
            }
            add_operation_token = {
                tag = RAJ
                token = E_token_navy
            }
            add_operation_token = {
                tag = RAJ
                token = E_token_airforce
            }
        }
        if = {
            limit = { NOT = { tag = AST }}
            add_operation_token = {
                tag = AST
                token = E_token_civilian
            }
            add_operation_token = {
                tag = AST
                token = E_token_army
            }
            add_operation_token = {
                tag = AST
                token = E_token_navy
            }
            add_operation_token = {
                tag = AST
                token = E_token_airforce
            }
        }
    }
}

TIM_FRA_up_africa = {
    if = {
        limit = { has_idea = FRA_africa_4 }
        swap_ideas = {
            remove_idea = FRA_africa_4
            add_idea = FRA_africa_5
        }
    }
    if = {
        limit = { has_idea = FRA_africa_3 }
        swap_ideas = {
            remove_idea = FRA_africa_3
            add_idea = FRA_africa_4
        }
    }
    if = {
        limit = { has_idea = FRA_africa_2 }
        swap_ideas = {
            remove_idea = FRA_africa_2
            add_idea = FRA_africa_3
        }
    }
    if = {
        limit = { has_idea = FRA_africa_1 }
        swap_ideas = {
            remove_idea = FRA_africa_1
            add_idea = FRA_africa_2
        }
    }
    if = {
        limit = {
            NOT = {
                OR = {
                    has_idea = FRA_africa_1
                    has_idea = FRA_africa_2
                    has_idea = FRA_africa_3
                    has_idea = FRA_africa_4
                    has_idea = FRA_africa_5
                }
            }
        }
        add_ideas = FRA_africa_1
    }
}
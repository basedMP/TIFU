FRA_spanish_intervention_category = {
	priority = 10
	icon = GFX_decision_category_military_operation

	allowed = {
        tag = FRA
    }
    available = {
        has_completed_focus = FRA_intervention_in_spain
    }
}
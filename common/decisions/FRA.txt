
FRA_spanish_intervention_category = {
	FRA_invite_FROM_to_non_intervention = {
		available = {
			has_country_flag = FRA_non_intervention
		}
		target_array = global.majors

		target_trigger = {
			FROM = {
				exists = yes
				NOT = { 
					has_country_flag = FRA_non_intervention_agreed 
					has_country_flag = FRA_non_intervention_refused
				}
				capital_scope = {
					is_on_continent = europe
				}
			}
		}
		visible = {
			has_country_flag = FRA_non_intervention
		}
		target_root_trigger = {
			has_country_flag = FRA_non_intervention	
		}
		cost = 25
		fire_only_once = yes
		ai_will_do = {
			factor = 1
			modifier = {
				not = { has_government = democratic }
				factor = 0
			}
			modifier = {
				has_stability < 0.3
				factor = 5
			}
		}
		complete_effect = {
			FROM = {
				country_event = lar_non_intervention.1
			}
		}
	}

	FRA_allow_non_military_aid = {
		available = {  }

		visible = { not = { has_country_flag = FRA_non_military_aid_allowed } }

		cancel_trigger = {
			OR = {
				has_global_flag = scw_over
				not = { has_country_flag = FRA_non_military_aid_allowed }
			}
		}

		complete_effect = { 
			set_country_flag = FRA_non_military_aid_allowed 
			clr_country_flag = FRA_non_intervention
			custom_effect_tooltip = FRA_non_military_aid_tt
			unlock_decision_tooltip = FRA_ban_non_military_aid
		}

		cost = 25
		ai_will_do = {
			factor = 1
			modifier = {
				has_stability < 0.3
				factor = 0
			}
		}
		days_remove = -1
		modifier = {
			stability_factor = -0.05
		}
	}

	FRA_ban_non_military_aid = {
		available = {  }

		visible = { has_country_flag = FRA_non_military_aid_allowed }

		complete_effect = { 
			clr_country_flag = FRA_non_military_aid_allowed 
			set_country_flag = FRA_non_intervention
		}

		cost = 0
		ai_will_do = {
			factor = 0
			modifier = {
				has_stability < 0.3
				add = 5
			}
		}
	}

	FRA_send_non_military_aid_to_FROM = {
		available = { 
			has_equipment = {
				support_equipment > 99
			} 
		}

		visible = { has_country_flag = FRA_non_military_aid_allowed }
		target_root_trigger = { has_country_flag = FRA_non_military_aid_allowed }

		targets = { SPA SPR SPB SPC SPD }

		target_trigger = {
			FROM = {
				exists = yes
				has_war = yes
				NOT = { has_war_with = ROOT }
			}
		}
		days_re_enable = 30
		complete_effect = { 
			send_equipment = {
				type = support_equipment
				amount = 100
				target = FROM
			}
		}
		ai_will_do = {
			factor = 1
			modifier = {
				FROM = { has_government = ROOT }
				factor = 5
			}
		}
		cost = 0
	}
	FRA_allow_arms_purchases = {
		available = {  }

		visible = { not = { has_country_flag = FRA_arms_purchases_allowed } }

		cancel_trigger = {
			OR = {
				has_global_flag = scw_over
				not = { has_country_flag = FRA_arms_purchases_allowed }
			}
		}

		complete_effect = { 
			set_country_flag = FRA_arms_purchases_allowed 
			clr_country_flag = FRA_non_intervention
			custom_effect_tooltip = FRA_allow_arms_purchases_tt
			unlock_decision_tooltip = FRA_ban_arms_purchases
		}

		cost = 25
		ai_will_do = {
			factor = 1
			modifier = {
				has_stability < 0.4
				factor = 0
			}
		}
		days_remove = -1
		modifier = {
			stability_factor = -0.1
			war_support_factor = 0.05
		}

	}

	FRA_ban_arms_purchases = {
		available = {  }

		visible = { has_country_flag = FRA_arms_purchases_allowed }

		complete_effect = { 
			clr_country_flag = FRA_arms_purchases_allowed 
			set_country_flag = FRA_non_intervention
		}

		cost = 0
		ai_will_do = {
			factor = 0
			modifier = {
				has_stability < 0.4
				add = 5
			}
		}
	}

	FRA_allow_sending_weapons = {
		available = {  }

		visible = { not = { has_country_flag = FRA_sending_weapons_allowed } }

		cancel_trigger = {
			OR = {
				has_global_flag = scw_over
				not = { has_country_flag = FRA_sending_weapons_allowed }
			}
		}

		complete_effect = { 
			set_country_flag = FRA_sending_weapons_allowed 
			clr_country_flag = FRA_non_intervention
			unlock_decision_tooltip = FRA_ban_sending_weapons
		}

		cost = 25
		days_remove = -1
		modifier = {
			stability_factor = -0.15
			war_support_factor = 0.075
			lend_lease_tension = -1
			request_lease_tension = -1
		}
		ai_will_do = {
			factor = 1
			modifier = {
				has_stability < 0.45
				factor = 0
			}
		}
	}

	FRA_ban_sending_weapons = {
		available = {  }

		visible = { has_country_flag = FRA_sending_weapons_allowed }

		complete_effect = { 
			clr_country_flag = FRA_sending_weapons_allowed 
		}

		cost = 0
		ai_will_do = {
			factor = 0
			modifier = {
				has_stability < 0.45
				add = 5
			}
		}
	}
	FRA_allow_volunteer_work = {
		available = {  }

		visible = { not = { has_country_flag = FRA_volunteer_work_allowed } }

		cancel_trigger = {
			OR = {
				has_global_flag = scw_over
				not = { has_country_flag = FRA_volunteer_work_allowed }
			}
		}

		complete_effect = { 
			set_country_flag = FRA_volunteer_work_allowed 
			clr_country_flag = FRA_non_intervention
			unlock_decision_tooltip = FRA_ban_volunteer_work
			every_other_country = {
				limit = {
					OR = {
						tag = SPA
						tag = SPD
						tag = SPB
						tag = SPC
					}
					exists = yes
					NOT = { has_country_flag = FRA_has_received_volunteers }
				}
				country_event = lar_france_volunteers.1
			}
			custom_effect_tooltip = FRA_allow_volunteer_work_tt
		}

		cost = 25
		ai_will_do = {
			factor = 1
			modifier = {
				has_stability < 0.5
				factor = 0
			}
		}
		days_remove = -1
		modifier = {
			stability_factor = -0.2
			war_support_factor = 0.1
		}
	}

	FRA_ban_volunteer_work = {
		available = {  }

		visible = { has_country_flag = FRA_volunteer_work_allowed }

		complete_effect = { 
			clr_country_flag = FRA_volunteer_work_allowed 
		}
		ai_will_do = {
			factor = 0
			modifier = {
				has_stability < 0.5
				add = 5
			}
		}
		cost = 0
	}
	FRA_allow_covert_intervention = {
		available = {  }

		visible = { not = { has_country_flag = FRA_covert_intervention_allowed } }

		cancel_trigger = {
			OR = {
				has_global_flag = scw_over
				not = { has_country_flag = FRA_covert_intervention_allowed }
			}
		}

		complete_effect = { 
			set_country_flag = FRA_covert_intervention_allowed 
			clr_country_flag = FRA_non_intervention
			set_rule = { can_send_volunteers = yes }
		}

		cost = 25
		ai_will_do = {
			factor = 1
			modifier = {
				has_stability < 0.55
				factor = 0
			}
			modifier = {
				has_stability < 0.8
				factor = 0.5
			}
		}
		days_remove = -1
		modifier = {
			stability_factor = -0.25
			war_support_factor = 0.125
			send_volunteers_tension = -0.9
		}
	}

	FRA_ban_covert_intervention = {
		available = { 
			NOT = {
				any_other_country = {
					has_volunteers_amount_from = {
						tag = ROOT
						count > 0
					}
				}
			}
		}

		visible = { has_country_flag = FRA_covert_intervention_allowed }

		complete_effect = { 
			clr_country_flag = FRA_covert_intervention_allowed 
			#recall_volunteers_from = SPR
		}

		cost = 0
		ai_will_do = {
			factor = 0
			modifier = {
				has_stability < 0.55
				add = 5
			}
		}
	}
}

economy_decisions = {

	FRA_revoke_the_matignon_agreements = {
		visible = { has_idea = FRA_matignon_agreements }

		cost = 150

		ai_will_do = {
			factor = 10
		}

		complete_effect = {
			remove_ideas = FRA_matignon_agreements
		}
	}

	FRA_reorganize_aviation_industry_north = {
		available = {  }

		visible = { has_completed_focus = FRA_reorganize_the_aviation_industry }

		remove_effect = { 
			set_country_flag = FRA_SNCAN_formed
			random_owned_state = {
				limit = { 
					OR = {
						state = 29 
						state = 697 
						state = 15 
						state = 16
					}
					is_fully_controlled_by = ROOT 
					free_building_slots = {
						building = arms_factory
						size > 0
						include_locked = yes
					}
				}
				add_building_construction = {
					type = arms_factory
					level = 1
					instant_build = yes
				}
			}
		}
		days_remove = 120
		cost = 50
		fire_only_once = yes
		ai_will_do = {
			factor = 5
		}
	}
	FRA_reorganize_aviation_industry_west = {
		available = {  }

		visible = { has_completed_focus = FRA_reorganize_the_aviation_industry }

		remove_effect = { 
			set_country_flag = FRA_SNCAO_formed
			random_owned_state = {
				limit = { 
					OR = {
						state = 30
						state = 14
						state = 23
					}
					is_fully_controlled_by = ROOT 
					free_building_slots = {
						building = arms_factory
						size > 0
						include_locked = yes
					}
				}
				add_building_construction = {
					type = arms_factory
					level = 1
					instant_build = yes
				}
			}
		}
		days_remove = 120
		cost = 50
		fire_only_once = yes
		ai_will_do = {
			factor = 5
		}
	}
	FRA_reorganize_aviation_industry_center = {
		available = {  }

		visible = { has_completed_focus = FRA_reorganize_the_aviation_industry }

		remove_effect = { 
			set_country_flag = FRA_SNCAC_formed
			random_owned_state = {
				limit = { 
					OR = {
						state = 24
						state = 33
						state = 27
						state = 26
						state = 25
					}
					is_fully_controlled_by = ROOT 
					free_building_slots = {
						building = arms_factory
						size > 0
						include_locked = yes
					}
				}
				add_building_construction = {
					type = arms_factory
					level = 1
					instant_build = yes
				}
			}
		}
		days_remove = 120
		cost = 50
		fire_only_once = yes
		ai_will_do = {
			factor = 5
		}
	}
	FRA_reorganize_aviation_industry_south_east = {
		available = {  }

		visible = { has_completed_focus = FRA_reorganize_the_aviation_industry }

		remove_effect = { 
			set_country_flag = FRA_SNCASE_formed
			random_owned_state = {
				limit = { 
					OR = {
						state = 735
						state = 32
						state = 20
						state = 21
					}
					is_fully_controlled_by = ROOT 
					free_building_slots = {
						building = arms_factory
						size > 0
						include_locked = yes
					}
				}
				add_building_construction = {
					type = arms_factory
					level = 1
					instant_build = yes
				}
			}
		}
		days_remove = 120
		cost = 50
		fire_only_once = yes
		ai_will_do = {
			factor = 5
		}
	}
	FRA_reorganize_aviation_industry_south_west = {
		available = {  }

		visible = { has_completed_focus = FRA_reorganize_the_aviation_industry }

		remove_effect = { 
			set_country_flag = FRA_SNCASO_formed
			random_owned_state = {
				limit = { 
					OR = {
						state = 22
						state = 31
						state = 19
						state = 25
					}
					is_fully_controlled_by = ROOT 
					free_building_slots = {
						building = arms_factory
						size > 0
						include_locked = yes
					}
				}
				add_building_construction = {
					type = arms_factory
					level = 1
					instant_build = yes
				}
			}
		}
		days_remove = 120
		cost = 50
		fire_only_once = yes
		ai_will_do = {
			factor = 5
		}
	}
}
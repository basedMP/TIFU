### search_filters = {FOCUS_FILTER_POLITICAL}
### search_filters = {FOCUS_FILTER_RESEARCH}
### search_filters = {FOCUS_FILTER_INDUSTRY}
### search_filters = {FOCUS_FILTER_STABILITY}
### search_filters = {FOCUS_FILTER_WAR_SUPPORT}
### search_filters = {FOCUS_FILTER_MANPOWER}
### search_filters = {FOCUS_FILTER_ANNEXATION}
### search_filters = {FOCUS_FILTER_SPA_CIVIL_WAR}
### search_filters = {FOCUS_FILTER_SPA_CARLIST_UPRISING}
### search_filters = {FOCUS_FILTER_SPA_ANARCHIST_UPRISING}

focus_tree = {
	id = spanish_focus
	
	country = {
		factor = 0
		
		modifier = {
			add = 10
			tag = SPR
		}
	}

	default = no

	continuous_focus_position = { x = 50 y = 1000 }

	focus = {
		id = SPA_a_great_spain

		available = { }
		cancel_if_invalid = yes
		ai_will_do = {
			base = 10000
		}
		
		
		icon = GER_spanish_civil_war2
		x = 0
		y = 0
		cost = 5

		available_if_capitulated = yes
		search_filters = {FOCUS_FILTER_POLITICAL}
		completion_reward = {
			add_popularity = {
			    ideology = fascism
			    popularity = 0.1
			}
			add_popularity = {
			    ideology = neutrality
			    popularity = 0.05
			}
			add_political_power = 15
		}
	}

	
#  #  ##  ### ###  ##  #  #  ##  #   ###  ## ###  ##            ### ###   ##  #  #  ##   ##  
## # #  #  #   #  #  # ## # #  # #    #  #    #  #              #   #  # #  # ## # #  # #  # 
# ## ####  #   #  #  # # ## #### #    #   #   #   #      ##     ##  ###  #### # ## #    #  # 
#  # #  #  #   #  #  # #  # #  # #    #    #  #    #            #   #  # #  # #  # #  # #  # 
#  # #  #  #  ###  ##  #  # #  # ### ### ##   #  ##             #   #  # #  # #  #  ##   ##  

	focus = {
		id = SPA_unify_the_nationalist_front
		available = { }
		prerequisite = { focus = SPA_a_great_spain }
		
		icon = GFX_focus_spa_unify_the_nationalist_front
		x = 0
		y = 1
		relative_position_id = SPA_a_great_spain
		cost = 5

		available_if_capitulated = yes
		search_filters = {FOCUS_FILTER_STABILITY}
		completion_reward = {
			add_stability = -0.2
			custom_effect_tooltip = "Start the Spanish Civil War"
			set_politics = {
				ruling_party = fascism
				elections_allowed = no
			}
			remove_ideas = SPR_political_violence
			hidden_effect = {
				remove_ideas = SPR_national_strikes_1
				remove_ideas = SPR_national_strikes_2
				remove_ideas = SPR_national_strikes_3
				remove_ideas = SPR_national_strikes_4
				remove_ideas = SPR_national_strikes_5
				remove_ideas = SPR_national_strikes_6
				remove_ideas = SPR_national_strikes_7
				remove_ideas = SPR_national_strikes_8
				remove_ideas = SPR_national_strikes_9
			}
			add_ideas = SPR_national_strikes_5
			hidden_effect = {
				set_global_flag = spanish_civil_war
				BKB = {
					transfer_state = 173
					transfer_state = 169
					transfer_state = 170
					transfer_state = 788
					transfer_state = 41
					transfer_state = 793
					transfer_state = 166
					transfer_state = 794
					transfer_state = 165
					transfer_state = 167
					transfer_state = 168
					transfer_state = 175
					transfer_state = 789
					transfer_state = 792
					transfer_state = 790
				}
				set_country_flag = SPR_nationalist_spain_flag
			}
		}
	}

	focus = {
		id = SPA_the_corpo_truppe_volontarie
		bypass = {
		}
		prerequisite = { focus = SPA_unify_the_nationalist_front }

		ai_will_do = {
			factor = 1
		}
		
		icon = GFX_focus_befriend_italy
		x = 5
		y = 1
		relative_position_id = SPA_unify_the_nationalist_front
		cost = 5

		available_if_capitulated = yes
		search_filters = {FOCUS_FILTER_RESEARCH}
		completion_reward = {
			add_tech_bonus = {
				name = SPA_the_corpo_truppe_volontarie
				bonus = 1.0
				uses = 1
				category = infantry_weapons
			}
			add_tech_bonus = {
				name = special_bonus
				bonus = 1.0
				uses = 2
				category = mountaineers_tech
			}
			add_tech_bonus = {
				name = special_bonus
				bonus = 1.0
				uses = 2
				category = marine_tech
			}
			add_tech_bonus = {
				name = "Landing Crafts"
				bonus = 1
				uses = 2
				category = tp_tech
			}
		}
	}

	
	focus = {
		id = "Invitar a Expertos Italianos en Artillería"
		icon = ROM_arti2
		x = -1
		y = 1
		cost = 5
		ai_will_do = {
			factor = 5
		}
		prerequisite = { focus = SPA_the_corpo_truppe_volontarie }
		relative_position_id = SPA_the_corpo_truppe_volontarie
		available_if_capitulated = yes

		bypass = {
			
		}

		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			custom_effect_tooltip = "This will research all Arty and Rocket-Arty-Techs the Italians have or give a Research Bonus"
			if = {
				limit = { ITA = { has_tech = interwar_artillery } }
				set_technology = {
					interwar_artillery = 1
					popup = no
				}
			}
			else = {
				add_tech_bonus = {
					name = "Artillery Modernisation"
					bonus = 1.0
					uses = 1
					technology = interwar_artillery
				}
			}
			if = {
				limit = { ITA = { has_tech = artillery1 } }
				set_technology = {
					artillery1 = 1
					popup = no
				}
			}
			else = {
				add_tech_bonus = {
					name = "Artillery Modernisation"
					bonus = 0.75
					uses = 1
					technology = artillery1
				}
			}
			if = {
				limit = { ITA = { has_tech = artillery2 } }
				set_technology = {
					artillery2 = 1
					popup = no
				}
			}
			else = {
				add_tech_bonus = {
					name = "Artillery Modernisation"
					bonus = 0.70
					uses = 1
					technology = artillery2
				}
			}
			if = {
				limit = { ITA = { has_tech = artillery3 } }
				set_technology = {
					artillery3 = 1
					popup = no
				}
			}
			else = {
				add_tech_bonus = {
					name = "Artillery Modernisation"
					bonus = 0.65
					uses = 1
					technology = artillery3
				}
			}
			if = {
				limit = { ITA = { has_tech = artillery4 } }
				set_technology = {
					artillery4 = 1
					popup = no
				}
			}
			else = {
				add_tech_bonus = {
					name = "Artillery Modernisation"
					bonus = 0.60
					uses = 1
					technology = artillery4
				}
			}
			if = {
				limit = { ITA = { has_tech = artillery5 } }
				set_technology = {
					artillery5 = 1
					popup = no
				}
			}
			else = {
				add_tech_bonus = {
					name = "Artillery Modernisation"
					bonus = 0.55
					uses = 1
					technology = artillery5
				}
			}

			if = {
				limit = { ITA = { has_tech = rocket_artillery } }
				set_technology = {
					rocket_artillery = 1
					popup = no
				}
			}
			else = {
				add_tech_bonus = {
					name = "Artillery Modernisation"
					bonus = 0.5
					uses = 1
					technology = rocket_artillery
				}
			}
			if = {
				limit = { ITA = { has_tech = rocket_artillery2 } }
				set_technology = {
					rocket_artillery2 = 1
					popup = no
				}
			}
			else = {
				add_tech_bonus = {
					name = "Artillery Modernisation"
					bonus = 0.5
					uses = 1
					technology = rocket_artillery2
				}
			}
			if = {
				limit = { ITA = { has_tech = rocket_artillery3 } }
				set_technology = {
					rocket_artillery3 = 1
					popup = no
				}
			}
			else = {
				add_tech_bonus = {
					name = "Artillery Modernisation"
					bonus = 0.5
					uses = 1
					technology = rocket_artillery3
				}
			}

			if = {
				limit = { ITA = { has_tech = rocket_artillery4 } }
				set_technology = {
					rocket_artillery4 = 1
					popup = no
				}
			}
			else = {
				add_tech_bonus = {
					name = "Artillery Modernisation"
					bonus = 0.5
					uses = 1
					technology = rocket_artillery4
				}
			}
			
		}
	}

	focus = {
		id = SPA_the_condor_legion
		bypass = {
		}
		prerequisite = { focus = SPA_unify_the_nationalist_front }
		
		icon = GFX_focus_chi_mission_to_germany
		x = 7
		y = 1
		relative_position_id = SPA_unify_the_nationalist_front
		cost = 5

		ai_will_do = {
			factor = 1
		}

		available_if_capitulated = yes
		search_filters = {FOCUS_FILTER_RESEARCH}
		completion_reward = {
			add_tech_bonus = {
				name = SPA_the_condor_legion
				bonus = 1.0
				uses = 1
				category = armor
			}
			custom_effect_tooltip = available_political_advisor
			show_ideas_tooltip = SPA_wilhelm_ritter_von_thoma
		}
	}

	focus = {
		id = "Cañón Antitanque Alemán"
		icon = ROM_tanks2
		x = 1
		y = 1
		cost = 5
		ai_will_do = {
			factor = 5
		}
		prerequisite = { focus = SPA_the_condor_legion }
		relative_position_id = SPA_the_condor_legion
		available_if_capitulated = yes

		bypass = {
			
		}

		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			custom_effect_tooltip = "This will research all AT-Techs the Germans have or give a Research Bonus"
			if = {
				limit = { GER = { has_tech = interwar_antitank } }
				set_technology = {
					interwar_antitank = 1
					popup = no
				}
			}
			else = {
				add_tech_bonus = {
					name = "Anti Tank Modernisation"
					bonus = 1.0
					uses = 1
					technology = interwar_antitank
				}
			}
			if = {
				limit = { GER = { has_tech = antitank1 } }
				set_technology = {
					antitank1 = 1
					popup = no
				}
			}
			else = {
				add_tech_bonus = {
					name = "Anti Tank Modernisation"
					bonus = 0.90
					uses = 1
					technology = antitank1
				}
			}
			if = {
				limit = { GER = { has_tech = antitank2 } }
				set_technology = {
					antitank2 = 1
					popup = no
				}
			}
			else = {
				add_tech_bonus = {
					name = "Anti Tank Modernisation"
					bonus = 0.8
					uses = 1
					technology = antitank2
				}
			}
			if = {
				limit = { GER = { has_tech = antitank3 } }
				set_technology = {
					antitank3 = 1
					popup = no
				}
			}
			else = {
				add_tech_bonus = {
					name = "Anti Tank Modernisation"
					bonus = 0.7
					uses = 1
					technology = antitank3
				}
			}
			if = {
				limit = { GER = { has_tech = antitank4 } }
				set_technology = {
					antitank4 = 1
					popup = no
				}
			}
			else = {
				add_tech_bonus = {
					name = "Anti Tank Modernisation"
					bonus = 0.65
					uses = 1
					technology = antitank4
				}
			}
			if = {
				limit = { GER = { has_tech = antitank5 } }
				set_technology = {
					antitank5 = 1
					popup = no
				}
			}
			else = {
				add_tech_bonus = {
					name = "Anti Tank Modernisation"
					bonus = 0.55
					uses = 1
					technology = antitank5
				}
			}
			
		}
	}

	focus = {
		id = SPA_equipment_shipments
		prerequisite = { focus = SPA_the_corpo_truppe_volontarie focus = SPA_the_condor_legion }
		
		icon = USA_well_supplied_and_motivated2
		x = 1
		y = 1
		relative_position_id = SPA_the_corpo_truppe_volontarie
		cost = 5

		available_if_capitulated = yes

		completion_reward = {
			add_tech_bonus = {
				name = SPA_equipment_shipments
				bonus = 2.0
				uses = 1
				category = motorized_equipment
			}
			add_tech_bonus = {
				name = SPA_equipment_shipments
				bonus = 1.0
				uses = 2
				category = logistics_tech
			}
		}
	}

	focus = {
		id = SPA_obtain_training_staff
		prerequisite = { focus = SPA_equipment_shipments }
		
		icon = GFX_focus_generic_military_academy
		x = -1
		y = 1
		relative_position_id = SPA_equipment_shipments
		cost = 5

		available_if_capitulated = yes

		completion_reward = {
			army_experience = 100
		}
	}

	focus = {
		id = SPA_doctrinal_advancements
		prerequisite = { focus = SPA_equipment_shipments }
		
		icon = GFX_goal_generic_army_doctrines
		x = 1
		y = 1
		relative_position_id = SPA_equipment_shipments
		cost = 5

		available_if_capitulated = yes
		search_filters = {FOCUS_FILTER_RESEARCH}
		completion_reward = {
			add_doctrine_cost_reduction = {
				name = SPA_doctrinal_advancements
				cost_reduction = 0.5
				uses = 1
				category = land_doctrine
			}
			add_doctrine_cost_reduction = {
				name = SPA_doctrinal_advancements
				cost_reduction = 0.5
				uses = 1
				category = air_doctrine
			}
		}
	}

	focus = {
		id = SPA_la_division_azul
		available = {
			has_global_flag = scw_over
		}
		bypass = {
		}
		prerequisite = { focus = SPA_obtain_training_staff }
		prerequisite = { focus = SPA_doctrinal_advancements }
		
		icon = GFX_goal_rhineland
		x = 1
		y = 1
		relative_position_id = SPA_obtain_training_staff
		cost = 5

		available_if_capitulated = yes

		completion_reward = {
			add_ideas = HMM_vol_vol
			set_rule = {
				can_send_volunteers = yes
			}
		}
	}

	focus = {
		id = SPA_expand_conscription
		available = {
		}
		bypass = {
		}
		prerequisite = { focus = SPA_obtain_training_staff }
		
		icon = goal_USA_individual_replacement_system2
		x = -1
		y = 1
		relative_position_id = SPA_obtain_training_staff
		cost = 5

		available_if_capitulated = yes
		search_filters = {FOCUS_FILTER_MANPOWER}
		completion_reward = {
			add_ideas = SPA_expand_conscription
		}
	}

	focus = {
		id = SPR_gib_focus
		Text = "Death to all Stone-Stealing Nations"
		icon = SPR_gib
		x = 0
		y = 1
		cost = 1
		ai_will_do = {
			factor = 5
		}
		prerequisite = { focus = SPA_expand_conscription }
		relative_position_id = SPA_expand_conscription
		available_if_capitulated = yes

		available = {
			GER = { has_war_with = ENG }
		}

		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			add_timed_idea = {
				idea = SPR_revenge
				days = 90
			}
		}
	}

	focus = {
		id = "Fuerzas especiales"
		icon = GFX_goal_generic_allies_build_infantry
		x = 1
		y = 1
		cost = 5
		ai_will_do = {
			factor = 5
		}
		prerequisite = { focus = SPA_doctrinal_advancements }
		relative_position_id = SPA_doctrinal_advancements
		available_if_capitulated = yes

		bypass = {
			
		}

		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			add_doctrine_cost_reduction = {
				name = SPA_doctrinal_advancements
				cost_reduction = 0.5
				uses = 2
				category = land_doctrine
			}
		}
	}

	focus = {
		id = SPA_lessons_from_scw
		text = "Lessons from the Civil War"
		available = {
		}
		bypass = {
		}
		prerequisite = { focus = "Fuerzas especiales" }
		
		icon = r56_goal_spr_recruitment_drive2
		x = 0
		y = 1
		relative_position_id = "Fuerzas especiales"
		cost = 5

		available_if_capitulated = yes
		search_filters = {FOCUS_FILTER_MANPOWER}
		completion_reward = {
			add_ideas = SPA_lessons_from_scw
		}
	}


	focus = {
		id = SPA_save_the_alcazar
		available = {
		}
		prerequisite = { focus = SPA_consolidate_the_north }
		
		icon = GFX_focus_spa_save_the_alcazar
		x = -2
		y = 1
		relative_position_id = SPA_consolidate_the_north
		cost = 5

		available_if_capitulated = yes

		completion_reward = {
			army_experience = 50
			transfer_state = 788
			transfer_state = 41
		}
	}

	focus = {
		id = SPA_martyrdom_for_primo_de_rivera
		available = {
		}
		bypass = {
			has_global_flag = scw_over
		}
		prerequisite = { focus = SPA_save_the_alcazar }
		
		icon = GFX_goal_generic_secret_weapon
		x = -1
		y = 1
		relative_position_id = SPA_save_the_alcazar
		cost = 5

		available_if_capitulated = yes
		search_filters = {FOCUS_FILTER_WAR_SUPPORT FOCUS_FILTER_POLITICAL}
		completion_reward = {
			army_experience = 25
			add_war_support = 0.1
			add_popularity = {
			    ideology = fascism
			    popularity = 0.05
			}
			transfer_state = 170
		}
	}

	focus = {
		id = SPA_caudillo_of_spain
		available = {
			controls_state = 41
		}
		prerequisite = { focus = SPA_save_the_alcazar }
		
		icon = SPR_franco
		x = 1
		y = 1
		relative_position_id = SPA_save_the_alcazar
		cost = 5

		available_if_capitulated = yes
		search_filters = {FOCUS_FILTER_STABILITY}
		completion_reward = {
			add_stability = 0.05
			add_country_leader_role = {
				character = "SPA_francisco_franco"
				country_leader = {
				  ideology=fascism_ideology
				  traits = { caudillo_1 }
				  expire="1965.1.1"
				}
				promote_leader = yes
			}
		}
	}

	focus = {
		id = SPA_extol_the_martyrs_of_the_war
		available = {
		}
		bypass = {
		}
		prerequisite = { focus = SPA_martyrdom_for_primo_de_rivera }
		prerequisite = { focus = SPA_caudillo_of_spain }
		
		icon = GFX_goal_generic_propaganda
		x = 1
		y = 1
		relative_position_id = SPA_martyrdom_for_primo_de_rivera
		cost = 5

		available_if_capitulated = yes
		search_filters = {FOCUS_FILTER_WAR_SUPPORT FOCUS_FILTER_SPA_CARLIST_UPRISING}
		completion_reward = {
			army_experience = 25
			add_war_support = 0.1
			transfer_state = 169
			169 = {
				add_extra_state_shared_building_slots = 3
				add_building_construction = {
					type = arms_factory
					level = 2
					instant_build = yes
				}
			}
		}
	}

	focus = {
		id = SPA_consolidate_the_north
		available = { }
		prerequisite = { focus = SPA_unify_the_nationalist_front }
		
		icon = SPR_church
		x = 0
		y = 1
		relative_position_id = SPA_unify_the_nationalist_front
		cost = 5

		available_if_capitulated = yes
		search_filters = {FOCUS_FILTER_INDUSTRY}
		completion_reward = {
			army_experience = 50
			transfer_state = 790
			transfer_state = 792
			transfer_state = 166
			add_ideas = SPA_carlism_1
		}
	}

	focus = {
		id = SPA_foment_a_carlist_split
		available = {
		}
		bypass = {
			has_global_flag = SPR_carlist_uprising_flag
		}
		prerequisite = { focus = SPA_consolidate_the_north }
		
		icon = GFX_focus_spa_eliminate_the_carlists
		x = 0
		y = 1
		relative_position_id = SPA_consolidate_the_north
		cost = 5

		available_if_capitulated = yes
		search_filters = {FOCUS_FILTER_STABILITY FOCUS_FILTER_SPA_CARLIST_UPRISING}
		completion_reward = {
			swap_ideas = {
				remove_idea = SPA_carlism_1
				add_idea = SPA_carlism_2
			}
		}
	}

	focus = {
		id = SPA_a_methodical_approach
		available = {
		}
		bypass = {
			has_global_flag = scw_over
		}
		prerequisite = { focus = SPA_consolidate_the_north }
		
		icon = GFX_goal_generic_more_territorial_claims
		x = 2
		y = 1
		relative_position_id = SPA_consolidate_the_north
		cost = 5

		available_if_capitulated = yes

		completion_reward = {
			army_experience = 50
			transfer_state = 794
		}
	}

	focus = {
		id = SPA_tackle_the_vulnerable_fronts
		available = {
		}
		bypass = {
			has_global_flag = scw_over
		}
		prerequisite = { focus = SPA_a_methodical_approach }
		
		icon = GFX_goal_generic_major_war
		x = 1
		y = 1
		relative_position_id = SPA_a_methodical_approach
		cost = 5

		available_if_capitulated = yes

		completion_reward = {
			army_experience = 50
			transfer_state = 167
			transfer_state = 168
		}
	}

	focus = {
		id = SPA_integrate_the_requetes
		available = {}
		prerequisite = { focus = SPA_a_methodical_approach }
		
		icon = GFX_goal_generic_military_sphere
		x = -1
		y = 1
		relative_position_id = SPA_a_methodical_approach
		cost = 5

		available_if_capitulated = yes
		search_filters = {FOCUS_FILTER_MANPOWER}
		completion_reward = {
			army_experience = 50
			add_popularity = {
			    ideology = neutrality
			    popularity = 0.1
			}
			transfer_state = 793
			transfer_state = 175
			
		}
	}

	focus = {
		id = SPA_nail_in_the_coffin
		text = "Nail the Coffin"
		available = {
		}
		bypass = {
		}
		prerequisite = { focus = SPA_integrate_the_requetes }
		prerequisite = { focus = SPA_tackle_the_vulnerable_fronts }
		
		icon = SPR_ger_help2
		x = 1
		y = 1
		relative_position_id = SPA_integrate_the_requetes
		cost = 5

		available_if_capitulated = yes
		search_filters = {FOCUS_FILTER_RESEARCH}
		completion_reward = {
			transfer_state = 165
			165 = {
				add_extra_state_shared_building_slots = 3
				add_building_construction = {
					type = arms_factory
					level = 2
					instant_build = yes
				}
			}
		}
	}

	focus = {
		id = SPA_fuse_the_parties_fake
		text = "Fuse the Parties"
		available = {
			has_idea = SPA_carlism_2
		}
		prerequisite = { focus = SPA_foment_a_carlist_split }
		prerequisite = { focus = SPA_extol_the_martyrs_of_the_war }
		prerequisite = { focus = SPA_nail_in_the_coffin }
		
		icon = GFX_focus_spa_fuse_the_parties
		x = 0
		y = 3
		relative_position_id = SPA_foment_a_carlist_split
		cost = 10

		available_if_capitulated = yes
		search_filters = {FOCUS_FILTER_MANPOWER}
		completion_reward = {
			complete_national_focus = SPA_fuse_the_parties
		}
	}

	focus = {
		id = SPA_fuse_the_parties
		available = {
			always = no
		}
		prerequisite = { focus = SPA_unify_the_nationalist_front }
		
		icon = GFX_focus_spa_fuse_the_parties
		x = 13
		y = 1
		relative_position_id = SPA_unify_the_nationalist_front
		cost = 10

		available_if_capitulated = yes
		search_filters = {FOCUS_FILTER_MANPOWER}
		completion_reward = {
			army_experience = 100
			add_stability = 0.25
			transfer_state = 173
			transfer_state = 789
			set_party_name = { ideology = fascism long_name = SPR_fascism_francoist_party_long name = SPR_fascism_francoist_party }
			set_party_name = { ideology = neutrality long_name = SPR_fascism_francoist_party_long name = SPR_fascism_francoist_party }
			swap_ideas = {
				remove_idea = SPA_carlism_2
				add_idea = SPA_carlism_3
			}
			add_timed_idea = { idea = SPA_recovering_from_civil_war days = 1825 }
			set_global_flag = nationalist_victory
			set_global_flag = scw_over
			custom_effect_tooltip = "This will end the Spanish Civil War"
			country_event = { id = lar_news.1 hours = 6 }
		}
	}

	focus = {
		id = SPA_autarky
		available = {
		}
		bypass = {
			has_idea = free_trade
		}
		prerequisite = { focus = SPR_germany_program focus = SPR_italy_program }
		
		icon = SPR_legacy
		x = -2
		y = 2
		relative_position_id = SPA_fuse_the_parties
		cost = 5

		available_if_capitulated = yes
		search_filters = {FOCUS_FILTER_RESEARCH}
		completion_reward = {
			add_ideas = free_trade
			add_tech_bonus = {
				name = HOL_expand_curacao_oil_refineries
				bonus = 1.0
				uses = 3
				category = synth_resources
			}
		}
	}

	focus = {
		id = SPA_reduce_reliance_on_foreign_resources
		available = {
		}
		bypass = {
		}
		prerequisite = { focus = SPA_autarky }
		
		icon = Steel2
		x = -1
		y = 1
		relative_position_id = SPA_autarky
		cost = 10

		available_if_capitulated = yes
		search_filters = {FOCUS_FILTER_RESEARCH}
		completion_reward = {
			add_tech_bonus = {
				name = SPA_reduce_reliance_on_foreign_resources
				bonus = 1.5
				uses = 3
				category = excavation_tech
			}
		}
	}

	focus = {
		id = SPA_expand_the_axis_gold_trade
		available = {
		}
		bypass = {
		}
		prerequisite = { focus = SPA_reduce_reliance_on_foreign_resources }
		
		icon = GFX_focus_usa_reestablish_the_gold_standard
		x = 0
		y = 2
		relative_position_id = SPA_autarky
		cost = 10

		available_if_capitulated = yes

		completion_reward = {
			add_ideas = SPA_expand_the_axis_gold_trade
		}
	}

	focus = {
		id = SPA_mils69420
		text = "Lass die Affen aus dem Zoo"
		available = {
		}
		bypass = {
		}
		prerequisite = { focus = SPA_expand_the_axis_gold_trade }
		
		icon = SPR_civs
		x = 1
		y = 1
		relative_position_id = SPA_expand_the_axis_gold_trade
		cost = 10

		available_if_capitulated = yes

		completion_reward = {
			165 = {
				add_extra_state_shared_building_slots = 10
				add_building_construction = {
					type = arms_factory
					level = 4
					instant_build = yes
				}
			}
		}
	}

	focus = {
		id = SPA_national_recovery
		bypass = {
		}
		prerequisite = { focus = SPR_germany_program focus = SPR_italy_program }
		
		icon = EGY_recover_from_africa
		x = 0
		y = 2
		relative_position_id = SPA_fuse_the_parties
		cost = 10

		available_if_capitulated = yes
		search_filters = {FOCUS_FILTER_INDUSTRY}
		completion_reward = {
			add_ideas = SPA_national_recovery_1
		}
	}

	focus = {
		id = SPA_national_recovery_2
		text = "Expand National Workers Rights Program"
		bypass = {
		}
		prerequisite = { focus = SPA_national_recovery }
		
		icon = SPR_pay
		x = 1
		y = 1
		relative_position_id = SPA_national_recovery
		cost = 10

		available_if_capitulated = yes
		search_filters = {FOCUS_FILTER_INDUSTRY}
		completion_reward = {
			165 = {
				add_extra_state_shared_building_slots = 2
				add_building_construction = {
					type = industrial_complex
					level = 2
					instant_build = yes
				}
			}
		}
	}

	focus = {
		id = SPA_facilitate_pyrenees_freight_transports
		available = { }
		bypass = {
		}
		prerequisite = { focus = SPA_national_recovery }
		
		icon = SPR_infrastructure
		x = -1
		y = 1
		relative_position_id = SPA_national_recovery
		cost = 10

		available_if_capitulated = yes
		search_filters = {FOCUS_FILTER_INDUSTRY}
		completion_reward = {
			custom_effect_tooltip = SPA_facilitate_pyrenees_freight_transports_tt
			hidden_effect = {
				793 = {
					add_building_construction = {
						type = infrastructure
						level = 5
						instant_build = yes
					}
				}
				794 = {
					add_building_construction = {
						type = infrastructure
						level = 5
						instant_build = yes
					}
				}
				165 = {
					add_building_construction = {
						type = infrastructure
						level = 5
						instant_build = yes
					}
				}
				176 = {
					add_building_construction = {
						type = infrastructure
						level = 5
						instant_build = yes
					}
				}
				792 = {
					add_building_construction = {
						type = infrastructure
						level = 5
						instant_build = yes
					}
				}
				41 = {
					add_building_construction = {
						type = infrastructure
						level = 5
						instant_build = yes
					}
				}
			}
			build_railway = { #Madrid - Zaragoza - Pyrinnes
				path = {
					3938 9767 6993 9785 11766 813 3816 9813 11821 915
				}
			}
			build_railway = { #Zaragoza - Barcelona - French Border
				path = {
					3816 3954 13223 6966 6812 9764 854 9824 
				}
			}
			build_railway = { #Madrid - Valladolid - Burgos - Bilbao - French Border
				path = {
					3938 3794 9800 9846 11825 6936 903 885 11852 13233 740 6756
				}
			}
			swap_ideas = {
				remove_idea = SPA_national_recovery_1
				add_idea = SPA_national_recovery_2
			}
		}
	}

	focus = {
		id = SPA_strengthen_the_supreme_reality_of_spain
		available = {
		}
		bypass = {
		}
		prerequisite = { focus = SPA_adopt_carlist_policies }
		
		icon = GFX_focus_spa_strengthen_the_supreme_reality_of_spain
		x = -1
		y = 1
		relative_position_id = SPA_adopt_carlist_policies
		cost = 10

		available_if_capitulated = yes
		search_filters = {FOCUS_FILTER_STABILITY}
		completion_reward = {
			add_stability = 0.05
			41 = {
				add_extra_state_shared_building_slots = 10
				add_building_construction = {
					type = arms_factory
					level = 3
					instant_build = yes
				}
			}
		}
	}

	focus = {
		id = SPA_industrial_revolution
		text = "Industrial Revolution"
		
		available = {
			#
		}
		bypass = {
		}
		prerequisite = { focus = SPA_facilitate_pyrenees_freight_transports }
		prerequisite = { focus = SPA_national_recovery_2 }
		
		icon = SPR_war_eco
		x = 1
		y = 1
		relative_position_id = SPA_facilitate_pyrenees_freight_transports
		cost = 10

		available_if_capitulated = yes
		search_filters = {FOCUS_FILTER_INDUSTRY}
		completion_reward = {
			remove_ideas = SPA_recovering_from_civil_war
			
		}
	}

	focus = {
		id = SPA_the_duty_to_work
		#text = "The Duty to Work"
		available = {
			#
		}
		bypass = {
		}
		prerequisite = { focus = SPA_industrial_revolution }
		prerequisite = { focus = SPA_strengthen_the_supreme_reality_of_spain }
		prerequisite = { focus = SPA_mils69420 }
		
		icon = SPR_war_eco
		x = 0
		y = 2
		relative_position_id = SPA_industrial_revolution
		cost = 10

		available_if_capitulated = yes
		search_filters = {FOCUS_FILTER_INDUSTRY}
		completion_reward = {
			add_ideas = SPA_the_duty_to_work
		}
	}

	focus = {
		id = SPR_italy_program
		Text = "Stay true to the Original"
		icon =  SPR_take

		x = 1
		y = 1
		prerequisite = { focus = SPA_fuse_the_parties }
		mutually_exclusive = { focus = SPR_germany_program }
		relative_position_id = SPA_fuse_the_parties
		cost = 10
		ai_will_do = {
			factor = 5
		}
		
		available_if_capitulated = yes
	
		bypass = {
			
		}

		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			add_war_support = 0.10
			remove_ideas = SPR_national_strikes_5
			add_ideas = SPA_italy_program
		}
	}

	focus = {
		id = SPR_germany_program
		Text = "Adopt Nazism"
		icon =  GER_focus_background_1

		x = -1
		y = 1
		prerequisite = { focus = SPA_fuse_the_parties }
		mutually_exclusive = { focus = SPR_italy_program }
		relative_position_id = SPA_fuse_the_parties
		cost = 10
		ai_will_do = {
			factor = 5
		}
		
		available_if_capitulated = yes
	
		bypass = {
			
		}

		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			add_war_support = 0.1
			remove_ideas = SPR_national_strikes_5
			add_ideas = SPA_germany_program
		}
	}

	focus = {
		id = SPA_adopt_the_26_points
		available = {
			has_global_flag = scw_over
		}
		bypass = {
		}
		prerequisite = { focus = SPR_germany_program focus = SPR_italy_program }
		
		icon = SPR_points
		x = 2
		y = 2
		relative_position_id = SPA_fuse_the_parties
		cost = 10

		available_if_capitulated = yes
		search_filters = {FOCUS_FILTER_POLITICAL}
		completion_reward = {
			add_popularity = {
			    ideology = fascism
			    popularity = 0.1
			}
			add_war_support = 0.1
			if = {
				limit = {
					NOT = {
						has_idea = tot_economic_mobilisation
					}
				}
				add_ideas = tot_economic_mobilisation
			}
			else = {
				add_political_power = 150
			}
		}
	}

	focus = {
		id = SPA_direct_the_universities
		available = {
			num_of_factories > 50
			has_global_flag = scw_over
		}
		bypass = {
		}
		prerequisite = { focus = SPA_la_division_azul }
		
		icon = GFX_focus_research2
		x = 0
		y = 1
		relative_position_id = SPA_la_division_azul
		cost = 5

		available_if_capitulated = yes
		search_filters = {FOCUS_FILTER_RESEARCH}
		completion_reward = {
			add_research_slot = 1
		}
	}

	focus = {
		id = SPA_dictator_for_life
		available = {
			has_country_leader = {
				name = "Francisco Franco"
				ruling_only = yes
			}
		}
		bypass = {
		}
		prerequisite = { focus = SPA_adopt_the_26_points }
		mutually_exclusive = { focus = SPA_restore_the_monarchy }
		
		icon = GER_triumph
		x = 1
		y = 1
		relative_position_id = SPA_adopt_the_26_points
		cost = 10

		available_if_capitulated = yes
		search_filters = {FOCUS_FILTER_STABILITY}
		completion_reward = {
			add_stability = 0.05
			swap_ruler_traits = { remove = caudillo_1 add = caudillo_2 }
			hidden_effect = {
				if = {
					limit = {
						has_government = neutrality
					}
					remove_trait = {
						character = SPA_francisco_franco
						ideology = fascism_ideology
						trait = caudillo_1
					}
					add_trait = {
						character = SPA_francisco_franco
						ideology = fascism_ideology
						trait = caudillo_2
					}
				}
				if = {
					limit = {
						has_government = fascism
					}
					remove_trait = {
						character = SPA_francisco_franco
						ideology = despotism
						trait = caudillo_1
					}
					add_trait = {
						character = SPA_francisco_franco
						ideology = despotism
						trait = caudillo_2
					}
				}
			}
		}
	}

	focus = {
		id = SPA_adopt_carlist_policies
		text = "Adopt Carlist Policies"
		available = {
			has_idea = SPA_carlism_3
		}
		prerequisite = { focus = SPA_dictator_for_life }
		
		icon = GFX_focus_spa_fuse_the_parties
		x = -1
		y = 1
		relative_position_id = SPA_dictator_for_life
		cost = 10

		available_if_capitulated = yes
		search_filters = {FOCUS_FILTER_MANPOWER}
		completion_reward = {
			swap_ideas = {
				remove_idea = SPA_carlism_3
				add_idea = SPA_carlism_4
			}
		}
	}

	focus = {
		id = SPA_invest_in_naval_infrastructure
		prerequisite = { focus = SPA_unify_the_nationalist_front }

		available = {
			has_global_flag = scw_over
		}
		
		icon = GFX_goal_generic_navy_doctrines_tactics
		x = -3
		y = 1
		relative_position_id = SPA_unify_the_nationalist_front
		cost = 10

		available_if_capitulated = yes

		complete_tooltip = {
			every_state = {
				limit = { has_state_flag = SPA_invest_in_naval_infrastructure_3DY }
				add_extra_state_shared_building_slots = 3
				add_building_construction = {
					type = dockyard
					level = 3
					instant_build = yes
				}
			}
		}

		completion_reward = {
			navy_experience = 25
			random_owned_controlled_state = {
				limit = {
					free_building_slots = {
						building = dockyard
						size > 2
						include_locked = yes
					}
					is_core_of = ROOT
				}
				add_extra_state_shared_building_slots = 3
				add_building_construction = {
					type = dockyard
					level = 3
					instant_build = yes
				}
				set_state_flag = SPA_invest_in_naval_infrastructure_3DY
			}
		}
	}

	focus = {
		id = SPA_expand_the_slipways
		available = {
		}
		bypass = {
		}
		prerequisite = { focus = SPA_invest_in_naval_infrastructure }
		
		icon = GFX_goal_generic_construct_naval_dockyard
		x = -1
		y = 1
		relative_position_id = SPA_invest_in_naval_infrastructure
		cost = 10

		available_if_capitulated = yes

		complete_tooltip = {
			every_state = {
				limit = { has_state_flag = SPA_expand_the_slipways_3DY }
				add_extra_state_shared_building_slots = 3
				add_building_construction = {
					type = dockyard
					level = 3
					instant_build = yes
				}
			}
		}

		completion_reward = {
			random_owned_controlled_state = {
				limit = {
					free_building_slots = {
						building = dockyard
						size > 2
						include_locked = yes
					}
					is_core_of = ROOT
				}
				add_extra_state_shared_building_slots = 3
				add_building_construction = {
					type = dockyard
					level = 3
					instant_build = yes
				}
				set_state_flag = SPA_expand_the_slipways_3DY
			}
			random_owned_controlled_state = {
				limit = {
					free_building_slots = {
						building = dockyard
						size > 2
						include_locked = yes
					}
					is_core_of = ROOT
					NOT = { has_state_flag = SPA_expand_the_slipways_3DY }
				}
				add_extra_state_shared_building_slots = 3
				add_building_construction = {
					type = dockyard
					level = 3
					instant_build = yes
				}
				set_state_flag = SPA_expand_the_slipways_3DY
			}
		}
	}

	focus = {
		id = SPA_the_plan_imperial
		available = {
		}
		bypass = {
		}
		prerequisite = { focus = SPA_expand_the_slipways }
		
		icon = GFX_goal_generic_navy_battleship
		x = 0
		y = 2
		relative_position_id = SPA_expand_the_slipways
		cost = 10

		available_if_capitulated = yes
		search_filters = {FOCUS_FILTER_RESEARCH}
		completion_reward = {
			navy_experience = 50
			add_ideas = SPA_the_plan_imperial
			add_tech_bonus = {
				name = SPA_glory_and_wealth_on_the_sea_lanes
				bonus = 1
				uses = 1
				category = bb_tech
			}
			add_tech_bonus = {
				name = SPA_glory_and_wealth_on_the_sea_lanes
				bonus = 1
				uses = 1
				category = cv_tech
			}
		}
	}

	focus = {
		id = SPA_restart_small_scale_shipbuilding
		available = {
		}
		bypass = {
		}
		prerequisite = { focus = SPA_expand_the_slipways }
		
		icon = GFX_focus_generic_destroyer
		x = -2
		y = 2
		relative_position_id = SPA_invest_in_naval_infrastructure
		cost = 10

		available_if_capitulated = yes
		search_filters = {FOCUS_FILTER_RESEARCH}
		completion_reward = {
			navy_experience = 25
			add_tech_bonus = {
				name = SPA_restart_small_scale_shipbuilding
				bonus = 1
				uses = 1
				category = cl_tech
				category = ca_tech
			}
			add_tech_bonus = {
				name = SPA_restart_small_scale_shipbuilding
				bonus = 1
				uses = 1
				category = dd_tech
			}
			add_tech_bonus = {
				name = SPA_restart_small_scale_shipbuilding
				bonus = 1
				uses = 1
				category = ss_tech
			}
		}
	}

	focus = {
		id = SPA_defenses_against_strategic_bombing
		available = {
			has_global_flag = scw_over
		}
		prerequisite = { focus = SPA_unify_the_nationalist_front }
		
		icon = GFX_focus_generic_air_defense
		x = -5
		y = 1
		relative_position_id = SPA_unify_the_nationalist_front
		cost = 5

		available_if_capitulated = yes

		completion_reward = {
			every_owned_state = {
				limit = {
					is_controlled_by = ROOT
					is_core_of = ROOT
					is_in_home_area = yes
				}
				add_building_construction = {
					type = anti_air_building
					level = 5
					instant_build = yes
				}
			}
		}
	}

	focus = {
		id = SPA_defenses_against_invasion
		available = {
		}
		bypass = {
		}
		prerequisite = { focus = SPA_defenses_against_strategic_bombing }
		
		icon = ROM_dday2
		x = -1
		y = 1
		relative_position_id = SPA_defenses_against_strategic_bombing
		cost = 5

		available_if_capitulated = yes

		completion_reward = {
			custom_effect_tooltip = SPA_defenses_against_invasion_tt
			every_owned_state = {
				limit = {
					is_controlled_by = ROOT
					is_core_of = ROOT
					is_in_home_area = yes
				}
				add_building_construction = {
					type = coastal_bunker
					province = {
						all_provinces = yes
						limit_to_naval_base = yes
					}
					level = 2
					instant_build = yes
				}
				add_building_construction = {
					type = bunker
					province = {
						all_provinces = yes
						limit_to_naval_base = yes
					}
					level = 1
					instant_build = yes
				}
			}
		}
	}

}

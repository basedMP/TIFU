﻿division_template = {
	name = "50W Infantry (Training)"

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 0 y = 3 }
		infantry = { x = 0 y = 4 }

		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		infantry = { x = 1 y = 3 }
		infantry = { x = 1 y = 4 }

		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }
		infantry = { x = 2 y = 3 }
		infantry = { x = 2 y = 4 }

		infantry = { x = 3 y = 0 }
		infantry = { x = 3 y = 1 }
		infantry = { x = 3 y = 2 }
		infantry = { x = 3 y = 3 }
		infantry = { x = 3 y = 4 }

		infantry = { x = 4 y = 0 }
		infantry = { x = 4 y = 1 }
		infantry = { x = 4 y = 2 }
		infantry = { x = 4 y = 3 }
		infantry = { x = 4 y = 4 }

	}
	support = {

	}
}

units = {
	division= {
		location = 9904
		division_template = "50W Infantry (Training)"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
}
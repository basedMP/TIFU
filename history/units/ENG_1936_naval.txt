﻿units = {
	fleet = {
		name = "British Fleet I"
		naval_base = 11374 

		task_force = {
			name = "1st Strike Force" 
			location = 11374 

			# V Divisione Corazzate
			ship = { name = "HMS Pride" pride_of_the_fleet = yes definition = battleship start_experience_factor = 0.25 equipment = { ship_hull_heavy_1 = { amount = 1 owner = ENG version_name = "British Pride Class"  } } }
		}
	}
}
﻿units = {
	##### NAVAL UNITS #####
	
	fleet = {
		name = "Kriegsmarine"			
		naval_base = 241  # Wilhemshaven
		task_force = {				
			name = "Hochseeflotte"
			location = 241  # Wilhemshaven
			ship = { name = "Admiral Scheer" pride_of_the_fleet = yes definition = heavy_cruiser start_experience_factor = 0.3 equipment = { ship_hull_cruiser_panzerschiff = { amount = 1 owner = GER version_name = "Deutschland Class" } } }
		}
	}
}
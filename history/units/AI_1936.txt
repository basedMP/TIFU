﻿division_template = {
	name = "Inf Division"
	division_names_group = DEN_INF_01
	is_locked = yes

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 0 y = 3 }
		infantry = { x = 0 y = 4 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		infantry = { x = 1 y = 3 }
		infantry = { x = 1 y = 4 }
	}
	support = {
		engineer = { x = 0 y = 0 }
		artillery = { x = 0 y = 1 }
	}
}

### Starting Production ###
instant_effect = {
	add_equipment_production = {
		equipment = {
			type = infantry_equipment_1
			creator = ROOT
		}
		requested_factories = 10
		progress = 0.85
		efficiency = 100
	}
}

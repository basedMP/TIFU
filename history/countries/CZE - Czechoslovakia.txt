﻿capital = 9

set_oob = "AI_1936"
set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	tech_trucks = 1
	tech_support = 1
	tech_recon = 1
	tech_mountaineers = 1
	gw_artillery = 1
	interwar_antiair = 1
	fuel_silos = 1
	basic_train = 1
}
recruit_character = CZE_edvard_benesh

set_research_slots = 0

set_politics = {
	ruling_party = democratic
	last_election = "1935.5.19"
	election_frequency = 60
	elections_allowed = yes
}

set_popularities = {
	democratic = 65
	neutrality = 20
	fascism = 5
	communism = 10
}

add_ideas = HMM_dont_produce

﻿capital = 613 # Nanjing

oob = "CHI_1936"
if = {
	limit = { has_dlc = "Man the Guns" }
		set_naval_oob = "CHI_1936_naval_mtg"
	else = {
		set_naval_oob = "CHI_1936_naval_legacy"
	}
}
if = {
	limit = { has_dlc = "By Blood Alone" }
		set_air_oob = "CHI_1936_air_bba"
	else = {
		set_air_oob = "CHI_1936_air_legacy"
		set_technology = {
			early_fighter = 1
			early_bomber = 1
		}
	}
}

recruit_character = CHI_chiang_kaishek



recruit_character = CHI_tu_yuming
recruit_character = CHI_hsueh_yueh
recruit_character = CHI_tai_anlan
recruit_character = CHI_wang_yaowu
recruit_character = CHI_sun_li_jen
recruit_character = CHI_hu_zongnan
recruit_character = CHI_gu_zhutong
recruit_character = CHI_wei_lihuang
recruit_character = CHI_fu_zuoyi
recruit_character = CHI_zhang_xueliang
recruit_character = CHI_fang_zeyi

recruit_character = CHI_chen_cheng
recruit_character = CHI_li_zongren
recruit_character = CHI_bo_yibo
recruit_character = CHI_huang_shen

recruit_character = CHI_wang_shuming


recruit_character = CHI_xiao_yisu


recruit_character = CHI_yu_hanmou
recruit_character = CHI_he_yingqin
recruit_character = CHI_gao_zhihang




recruit_character = CHI_soong_mei_ling



set_stability = 0.2
set_war_support = 0.5
# Starting tech
set_technology = {
	infantry_weapons = 1
	tech_trucks = 1
	gw_artillery = 1
	interwar_antiair = 1
	mass_assault = 1
	fleet_in_being = 1
	basic_train = 1
}
if = {
	limit = {
		has_dlc = "No Step Back"
	}
	set_technology = {
		gwtank_chassis = 1
	}
	else = {
		set_technology = {
			gwtank = 1
		}
	}
}

add_ideas = {
	CHI_army_corruption_1
	CHI_ineffective_bureaucracy
	CHI_incompetent_officers
	free_trade
	CHI_hyper_inflation_1
	HMM_dont_produce # until CHI-JAP war
}

save_global_event_target_as = WTT_current_china_leader

set_convoys = 69

set_politics = {
	ruling_party = neutrality
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
	neutrality = 100
}
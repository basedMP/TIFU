﻿capital = 47

oob = "AI_1936"

set_cosmetic_tag = GRE_neutrality

set_research_slots = 0

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	tech_mountaineers = 1
	tech_support = 1		
	tech_engineers = 1
	tech_recon = 1
	tech_trucks = 1
	gw_artillery = 1
	trench_warfare = 1
	fuel_silos = 1
	synth_oil_experiments = 1
}

set_convoys = 10

set_politics = {
	ruling_party = neutrality
	last_election = "1935.6.9"
	election_frequency = 7
	elections_allowed = no
}
set_popularities = {
	democratic = 43
	fascism = 2
	communism = 17
	neutrality = 38
}

set_stability = 0.72
set_war_support = 0.23
set_convoys = 80

recruit_character = GRE_konstantinos_demertzis

add_ideas = HMM_dont_produce

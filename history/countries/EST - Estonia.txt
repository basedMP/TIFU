﻿capital = 812

oob = "AI_1936"

set_research_slots = 0

recruit_character = EST_konstantin_pats

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	tech_trucks = 1
	gw_artillery = 1
	interwar_antiair = 1
	tech_support = 1
	basic_train = 1
}

set_convoys = 5

set_politics = {
	ruling_party = neutrality
	last_election = "1932.5.21"
	election_frequency = 48
	elections_allowed = no
}
set_popularities = {
	democratic = 20
	fascism = 15
	communism = 5
	neutrality = 60
}

add_ideas = HMM_dont_produce

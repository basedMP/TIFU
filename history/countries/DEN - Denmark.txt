﻿capital = 37

oob = "AI_1936"

recruit_character = DEN_thorvald_stauning

set_research_slots = 0
set_stability = 0.9
set_war_support = 0.1
set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	tech_trucks = 1
	tech_support = 1		
	tech_recon = 1
	tech_engineers = 1
	gw_artillery = 1
	interwar_antiair = 1
	dispersed_industry = 1
	dispersed_industry2 = 1
	dispersed_industry3 = 1
}

set_politics = {
	ruling_party = democratic
	last_election = "1935.10.22"
	election_frequency = 48
	elections_allowed = yes
}
set_popularities = {
	democratic = 97
	fascism = 1
	communism = 2
}

set_convoys = 40

add_ideas = HMM_dont_produce

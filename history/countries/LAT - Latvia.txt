﻿capital = 808

oob = "AI_1936"

recruit_character = LAT_janis_balodis

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	gw_artillery = 1
	tech_support = 1
	basic_train = 1
}

set_research_slots = 0

set_convoys = 10

set_politics = {
	ruling_party = neutrality
	last_election = "1931.10.3"
	election_frequency = 10
	elections_allowed = no
}
set_popularities = {
	democratic = 30
	fascism = 15
	communism = 15
	neutrality = 40
}

add_ideas = HMM_dont_produce

capital = 271

set_oob = "ETH_1936"
set_variable = { ETH_war_escalation_level = 0 }
set_variable = { ETH_war_escalation_length = 123 } # Six months, starting in October

# Starting tech
set_technology = {
	infantry_weapons = 1
	tech_mountaineers = 1
	basic_train = 1
}

recruit_character = ETH_haile_selassie
recruit_character = ETH_haile_selassie_gugsa
recruit_character = ETH_alemework_beyene
recruit_character = ETH_ras_imru_haile_selassie
# Generals 
recruit_character = ETH_seyoum_mengesha
recruit_character = ETH_desta_damtew
recruit_character = ETH_ras_kassa 
# Political Advisors
recruit_character = ETH_makonnen_endelkatchew
# Military Advisors
recruit_character = ETH_john_robinson

set_global_flag = second_italo_ethiopian_war_flag
1936.05.05 = { clr_global_flag = second_italo_ethiopian_war_flag }

set_convoys = 5

set_politics = {
	ruling_party = neutrality
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}
set_popularities = {
	neutrality = 100
}
add_namespace = tim

#bootup
country_event = {
	id = tim.1
	title = ""
	desc = tim.1.d
	major = no
	
	is_triggered_only = yes
	
	option = {
		name = "ok cool"
	}
}

# SOV blet
country_event = {
	id = tim.2
	title = "German-Soviet Research Treaty"
	desc = "jooggling time lets go"
	major = no
	
	is_triggered_only = yes

	option = {
		name = "That sounds amazing! Lets hope this wont damage our great nation in the foreseeable Future!"
		custom_effect_tooltip = "You just got:"
		custom_effect_tooltip = "2x100% for Armor"
		custom_effect_tooltip = "1x250% for Mechanized 1"
	}
}


country_event = {
	id = tim.19375
	title = ""
	desc = ""
	major = no
	
	is_triggered_only = yes

	immediate = {
		if = {
			limit = {
				NOT = { has_global_flag = HMM_units_YUG }
			}
			set_global_flag = HMM_units_YUG
			set_temp_variable = { unitamnt = 40 }
			107 = {
				while_loop_effect = {
					limit = {
						check_variable = { unitamnt > 0 }
					}
					create_unit = {
						division = "division_template = \"Inf Division\" start_experience_factor = 0.4 start_equipment_factor = 1.0" 
						owner = YUG
					}
					subtract_from_temp_variable = { unitamnt = 1 }
				}
			}
		}
		if = {
			limit = {
				NOT = { has_global_flag = HMM_units_GRE }
			}
			set_global_flag = HMM_units_GRE
			set_temp_variable = { unitamnt = 24 }
			47 = {
				while_loop_effect = {
					limit = {
						check_variable = { unitamnt > 0 }
					}
					create_unit = {
						division = "division_template = \"Inf Division\" start_experience_factor = 0.4 start_equipment_factor = 1.0" 
						owner = GRE
					}
					subtract_from_temp_variable = { unitamnt = 1 }
				}
			}
		}
	}
	
	option = {
		name = "ok"
	}
}

country_event = {
	id = tim.1938
	title = ""
	desc = ""
	major = no
	
	is_triggered_only = yes

	immediate = {
		if = {
			limit = {
				NOT = { JAP = { has_war_with = CHI } }
			}
			JAP = { country_event = { id = tim.19381 } }
		}
	}
	
	option = {
		name = "ok"
	}
}
country_event = {
	id = tim.19381
	title = "Population Unhappy"
	desc = "The Population is unhappy with the current Situation of Sino-Japanese Relations, maybe the great Tennō should investigate."
	major = no
	
	is_triggered_only = yes

	immediate = {
		hidden_effect = { add_ideas = JAP_no_china_war }
		
	}
	
	option = {
		name = "The people want War!"
		add_ideas = JAP_no_china_war
	}
}
